<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion')->nullable();
            $table->string('email', 100)->nullable();
            $table->integer('nula');
            $table->integer('eliminado');
            $table->dateTime('fecha');
            $table->dateTime('fecha_de_creacion');
            $table->decimal('iva', 20, 2);
            $table->integer('numero');
            $table->integer('tipo_factura');
            $table->string('razon_social');
            $table->string('direccion')->nullable();
            $table->string('rut', 10);
            $table->string('telefono', 15)->nullable();
            $table->string('nombre_contacto')->nullable();
            $table->string('giro', 200);
            $table->boolean('exenta');
            $table->integer('id_empresa')->unsigned();

            $table->foreign('id_empresa')->references('id')->on('entities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas');
    }
}
