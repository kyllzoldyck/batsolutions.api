<?php

use Illuminate\Database\Seeder;

class FacturasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('facturas')->insert([
            [
                'descripcion' => 'Venta de bebidas',
                'email' => '',
                'nula' => 0,
                'eliminado' => 0,
                'fecha' => date('Y-m-d H:i:s'),
                'fecha_de_creacion' => date('Y-m-d H:i:s'),
                'iva' => 779,
                'numero' => 1,
                'tipo_factura' => 1,
                'razon_social' => 'Cliente 1 Razon Social',
                'direccion' => 'Av. Argentina',
                'rut' => '5238843-0',
                'telefono' => 2131231,
                'nombre_contacto' => 'Cliente 1',
                'id_empresa' => 1,
                'giro' => 'Cliente 1',
                'exenta' => 0
            ],
            [
                'descripcion' => 'Compra de chocolates',
                'email' => 'proveedor_chocolates@gmail.com',
                'nula' => 0,
                'eliminado' => 0,
                'fecha' => date('Y-m-d H:i:s'),
                'fecha_de_creacion' => date('Y-m-d H:i:s'),
                'iva' => 0,
                'numero' => 2,
                'tipo_factura' => 2,
                'razon_social' => 'Proveedor Chocolates 1',
                'direccion' => 'Av. Argentina',
                'rut' => '12170396-3',
                'telefono' => 2131231,
                'nombre_contacto' => 'Mr. Brasil',
                'id_empresa' => 1,
                'giro' => 'Cliente 1',
                'exenta' => 1
            ]
        ]);

        DB::table('detalle_factura')->insert([
            [
                'id_factura' => 1,
                'iva' => 570,
                'unidades' => 3,
                'descripcion_articulo' => 'coca cola 1L',
                'precio' => 1000,
                'codigo_articulo' => 'C100',
                'id_articulo' => 1
            ],
            [
                'id_factura' => 1,
                'iva' => 209,
                'unidades' => 1,
                'descripcion_articulo' => 'sprite 1L',
                'precio' => 1100,
                'codigo_articulo' => 'C100',
                'id_articulo' => 2
            ],
            [
                'id_factura' => 2,
                'iva' => 0,
                'unidades' => 2,
                'descripcion_articulo' => 'Chocolate Costa',
                'precio' => 1100,
                'codigo_articulo' => 'CH100',
                'id_articulo' => 3
            ],
            [
                'id_factura' => 2,
                'iva' => 0,
                'unidades' => 3,
                'descripcion_articulo' => 'coca cola 1L',
                'precio' => 1000,
                'codigo_articulo' => 'C100',
                'id_articulo' => 1
            ],
            [
                'id_factura' => 2,
                'iva' => 0,
                'unidades' => 1,
                'descripcion_articulo' => 'sprite 1L',
                'precio' => 1100,
                'codigo_articulo' => 'C100',
                'id_articulo' => 2
            ],
            [
                'id_factura' => 2,
                'iva' => 0,
                'unidades' => 2,
                'descripcion_articulo' => 'Chocolate Costa',
                'precio' => 1100,
                'codigo_articulo' => 'CH101',
                'id_articulo' => 3
            ]
        ]);
    }
}
