<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NotaCreditoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'descripcion' => 'required|max:255',
            'fecha' => 'required|date',
            'id_factura' => 'required|numeric',
            'numero' => 'required|numeric'    
        ];

        switch($this->method()) {
            case 'PUT':
                $rules = [
                    'descripcion' => 'required|max:255',
                    'fecha' => 'required|date'    
                ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'descripcion.required' => 'La descripción es obligatoria.',
            'descripcion.max:255' => 'La descripción supera el máximo de caracteres.',

            'fecha.required' => 'La fecha es obligatoria.',
            'fecha.date' => 'El valor de fecha no es correcto',
            
            'id_factura.required' => 'El ID de la factura es requerido.',
            'id_factura.numeric' => 'El ID de la factura no es correcto.',

            'numero.required' => 'El número de la factura es obligatorio.',
            'numero.numeric' => 'El número de la factura no es correcto.'
        ];
    }
}
