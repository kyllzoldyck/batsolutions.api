<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    
    protected $table = 'facturas';

    protected $fillables = [
        'descripcion',
        'email',
        'nula',
        'eliminado',
        'fecha',
        'fecha_de_creacion',
        'iva',
        'numero',
        'tipo_factura',
        'razon_social',
        'direccion',
        'rut',
        'telefono',
        'nombre_contacto',
        'giro',
        'exenta',
        'id_empresa'
    ];

    public $timestamps = false;

    public function notasCredito() {
        return $this->hasMany('App\NotaCredito', 'id_factura', 'id');
    }

    public function detallesFactura() {
        return $this->hasMany('App\DetalleFactura', 'id_factura', 'id');
    }

    public function empresa() {
        return $this->belongsTo('App\Entity', 'id_empresa');
    }

}
