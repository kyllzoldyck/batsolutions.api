<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\AuthController;
use App\Http\Requests\ProveedorRequest;
use App\Proveedor;
use App\Factura;
use Illuminate\Support\Facades\Log;
use \Freshwork\ChileanBundle\Rut;
use DB;

class ProveedorController extends Controller
{
    public function index()
    {
        try {
            Log::info('Getting proveedores');

            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
            

            if ($user->getStatusCode() != 200) 
            {
                Log::warning('Credenciales incorrectas');  
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }
            
            if ($user->getData()->id_rol == 1) 
            {
                Log::info('Admin: Getting proveedores');
                $proveedor = Proveedor::where('eliminado', 0)->orderBy('created_at', 'desc')->get();
            } 
            else {
                Log::info('Getting proveedores');
                $proveedor = Proveedor::where('eliminado', 0)
                    ->where('id_empresa', $user->getData()->id_empresa)
                    ->orderBy('created_at', 'desc')
                    ->get();
            }

            $proveedor->each(function (Proveedor $p) {
                $p->empresa;
            });
            
            Log::info('Proveedores obtained');
            return response()->json($proveedor, 200);
        } 
        catch (\Exception $e) 
        {
            Log::error($e);
            return response()->json($e, 404);
        }
    }

    public function store(ProveedorRequest $request)
    {
        try {

            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 401);
            }
            
            $rut = Rut::parse(mb_strtoupper(trim($request->input('rut')), 'UTF-8'))->format(Rut::FORMAT_WITH_DASH);

            $id_empresa = $user->getData()->id_rol == 1 ? trim($request->input('id_empresa')) : trim($user->getData()->id_empresa);

            $proveedor = Proveedor::where('rut', $rut)
                ->where('id_empresa', $id_empresa)
                ->first();

            if (count($proveedor) > 0)
                return response()->json(['error' => 'El Proveedor ya existe en el sistema.'], 400);

            $proveedor = new Proveedor();
            $proveedor->direccion = mb_strtolower(trim($request->input('direccion')), 'UTF-8');
            $proveedor->email = mb_strtolower(trim($request->input('email')), 'UTF-8');
            $proveedor->eliminado = 0;
            $proveedor->nombre_contacto = !empty($request->input('nombre_contacto')) ? mb_strtolower(trim($request->input('nombre_contacto')), 'UTF-8') : '';
            $proveedor->razon_social = mb_strtolower(trim($request->input('razon_social')), 'UTF-8');
            $proveedor->rut = $rut;
            $proveedor->telefono = trim($request->input('telefono'));
            $proveedor->giro = mb_strtolower(trim($request->input('giro')), 'UTF-8');
            $proveedor->id_empresa = $id_empresa;
            $proveedor->save();

            return response()->json($proveedor, 200);
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['created' => false, 'error' => 'Intentelo más tarde.'], 500);
        }
    }

    public function show($id)
    {
        try {
            $auth = new AuthController();
            if ($auth->getAuthenticatedUser()->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if (!is_numeric($id))
                return response()->json(['errors' => 'El ID no es numérico.', 200]);
            
            # 1 = Eliminado, 0 = No eliminado 
            $proveedor = Proveedor::where('id', $id)->where('eliminado', 0)->get();

            return response()->json($proveedor, 200);
        } catch (\Exception $e) {
            return response()->json($e, 500);
        }
    }

    public function update(ProveedorRequest $request, $id)
    {
        try {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            $proveedor = Proveedor::find($id);

            // $up = DB::table('proveedores')->where('id', 1)->update(['direccion' => $request->input('direccion'), 'email' => 'proveedortest123@proveedortest123.com', 'id_empresa' => 6, 'nombre_contacto' => 'proveedortest123', 'razon_social' => 'razon social proveedortest123', 'rut' => '12170396-3', 'telefono' => '123213213']);
            if (count($proveedor) == 0)
                return response()->json(['error' => 'El Proveedor no existe en el sistema.'], 404);

            $proveedorExistente = Proveedor::where('rut', mb_strtolower(trim($request->input('rut')), 'UTF-8'))
                ->where('id_empresa', $request->input('id_empresa') == $proveedor->id_empresa ? '' : $request->input('id_empresa'))
                ->first();

            if (count($proveedorExistente) > 0)
                return response()->json(['error' => 'Ya existe un proveedor con este RUT.'], 404);
           
            $proveedor->direccion = mb_strtolower(trim($request->input('direccion')), 'UTF-8');
            $proveedor->email = mb_strtolower(trim($request->input('email')), 'UTF-8');
            $proveedor->eliminado = 0;
            $proveedor->nombre_contacto = mb_strtolower(trim($request->input('nombre_contacto')), 'UTF-8');
            $proveedor->razon_social = mb_strtolower(trim($request->input('razon_social')), 'UTF-8');
            $proveedor->telefono = trim($request->input('telefono'));
            # $proveedor->rut = $request->input('rut');
            $proveedor->giro = mb_strtolower(trim($request->input('giro')), 'UTF-8');
            $proveedor->id_empresa = $user->getData()->id_rol == 1 ? trim($request->input('id_empresa')) : trim($user->getData()->id_empresa);
            $proveedor->save();

            return response()->json($proveedor, 200);
        } 
        catch (\Exception $e) {
            Log::error($e);
            return response()->json($e, 500);
        }
    }

    public function destroy($id)
    {
        try {

            $auth = new AuthController();
            if ($auth->getAuthenticatedUser()->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 401);
            }

            if (!is_numeric($id))
                return response()->json(['error' => 'El ID no es numérico.'], 422);

            $proveedor = Proveedor::find($id);
            
            if (count($proveedor) == 0)
                return response()->json(['error' => 'El proveedor no existe en el sistema.'], 400);
            
            $facturas = Factura::where('rut', $proveedor->rut)
                ->where('id_empresa', $proveedor->id_empresa)
                ->where('eliminado', 0)
                ->get();

            if (count($facturas) > 0) {
                return response()->json(['error' => 'El proveedor tiene facturas asociadas. No puede ser eliminado.'], 400);
            }    
            
            # 1 = Eliminado, 0 = No eliminado
            $proveedor->eliminado = 1;
            $proveedor->save();

            return response('Proveedor eliminado.', 204);
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['error' => 'Intentelo más tarde'], 500);
        }
    }

    public function getProveedoresByIdEmpresa($id)
    {
        try {

            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if (!is_numeric($id)) {
                return response()->json(['El ID debe ser numérico.'], 400);
            }

            $proveedores = [];

            if ($user->getData()->id_rol == 1) {
                $proveedores = Proveedor::where('eliminado', 0)->where('id_empresa', $id)->orderBy('created_at', 'desc')->get();
            }

            return response()->json($proveedores, 200);

        } catch (\Exception $e) {
            return response()->json($e, 500);
        }
    }

    public function getProveedorByRut($rut)
    {
        try {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if (empty($rut)) {
                return response()->json(['error' => 'El rut es obligatorio.'], 404);
            }

            $proveedor = [];

            if ($user->getData()->id_rol == 1) {
                $proveedor = Proveedor::where('rut', $rut)->where('eliminado', 0)->first();
            } else {
                $proveedor = Proveedor::where('rut', $rut)->where('eliminado', 0)->where('id_empresa', $user->getData()->id_empresa)->first();
            }

            return response()->json($proveedor, 200);

        } catch (\Exception $e) {

        }
    }
}
