<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PrintMenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id_menu' => 'required|numeric',
            'id_sub_menu' => 'required|numeric'
        ];

        switch ($this->method())
        {
            case 'PUT':
                array_push($rules, ['state' => 'required|numeric']);
                break; 
        }

        return $rules;
    }
}
