<?php

namespace App\Repository;

use App\User;

class ForgotPasswordTemplateEmail {

    public function forgotPasswordTemplate(User $user, $token)
    {
        return '
            <div>
                <h5>Restablecer contraseña</h5>
                <p>
                    Estimado <strong>' . $user->nombre . '</strong>, para poder restablecer su contraseña es necesario que se dirija al siguiente link: <br>
                    <a href="'. APP_DOMAIN_FRONT .'#/auth/password-reset?t=' . $token . '">Haga click aquí para restablecer su contraseña.</a>
                    <br>
                    <br>
                    <table>
                        <thead>
                            <th>Datos del usuario</th>
                            <th></th>
                        </thead>
                        <tbody>
                            <tr>
                                <td><strong>Nombre de usuario: </strong></td>
                                <td>'. $user->username .'</td>
                            </tr>
                            <tr>
                                <td><strong>Empresa:</strong></td>
                                <td>'. $user->empresa->razon_social .'</td>
                            </tr>
                            <tr>
                                <td><strong>Estado:</strong></td>
                                <td>'. ($user->activo == 1 ? 'Activo' : 'Inactivo') .'</td>
                            </tr>
                        </tbody>
                    </table>
                </p>
                <br>
                <strong>Si ha recibido este correo por equivocación, por favor ignorelo y borrelo de su bandeja de entrada.</strong>
                <br>
                <strong>Atte:</strong> Bat Solutions.
            </div>
        ';
    }

}