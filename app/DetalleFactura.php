<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleFactura extends Model
{
    
    protected $table = 'detalle_factura';

    protected $fillable = [
        'id_factura',
        'iva',
        'unidades',
        'descripcion_articulo',
        'precio',
        'codigo_articulo',
        'id_articulo'
    ];

    public $timestamps = false;

    public function factura() {
        return $this->belongsTo('App\Factura', 'id_factura');
    }

}
