<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Presupuesto;
use App\DetallePresupuesto;
use App\Cliente;
use App\Impuesto;
use App\Http\Requests\PresupuestoRequest;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Api\AuthController;
use App\Mail\PresupuestoMail;
use App\Repository\MakeReporte;
use Illuminate\Support\Facades\Mail;
use App\Repository\PresupuestoTemplateEmail;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class PresupuestoController extends Controller
{
    public function index()
    {
        try
        {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
            
            if ($user->getStatusCode() != 200) 
            {
                Log::warning('Credenciales incorrectas');    
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if ($user->getData()->id_rol == 1)
            {
                $presupuestos = Presupuesto::where('eliminado', 0)
                    ->orderBy('created_at', 'desc')
                    ->get();               
            }
            else 
            {
                $presupuestos = Presupuesto::where('eliminado', 0)->where('id_empresa', $user->getData()->id_empresa)->orderBy('created_at', 'desc')->get();
            }
            
            for ($i = 0; $i < count($presupuestos); $i++)
            {
                $detalles = $presupuestos[$i]->detallesPresupuesto;

                foreach($detalles as $detalle) 
                {
                    $presupuestos[$i]->sub_total += $detalle->unidades * $detalle->precio;
                    $presupuestos[$i]->total += $presupuestos[$i]->exenta == 1 ? ($detalle->unidades * $detalle->precio) : ($detalle->unidades * $detalle->precio) + $detalle->iva;
                }
            } 
        }
        catch (\Exception $e)
        {
            Log::error($e);
            return respose()->json(['get' => false], 500);
        }

        Log::info('Presupuestos obtained');
        return response()->json($presupuestos, 200);
    }

    public function store(PresupuestoRequest $request)
    {
        $rutCliente = trim($request->input('cliente_rut'));
        $fechaExpiracion = date($request->input('fecha_expiracion'));
        $nombre = mb_strtolower(trim($request->input('nombre')), 'UTF-8');
        $email = mb_strtolower(trim($request->input('email')), 'UTF-8');
        $asunto = mb_strtolower(trim($request->input('asunto')), 'UTF-8');
        $descripcion = mb_strtolower(trim($request->input('descripcion')), 'UTF-8');
        $detallePresupuesto = $request->input('detalles_presupuesto');
        $exenta = $request->input('exenta');
    
        try
        {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
            
            if ($user->getStatusCode() != 200) 
            {
                Log::warning('Credenciales incorrectas');    
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            $idEmpresa = $user->getData()->id_empresa == 1 ? $request->input('id_empresa') : $user->getData()->id_empresa;

            if ($user->getData()->id_rol == 1) 
            {
                $cliente = Cliente::where('rut', $rutCliente)->where('eliminado', 0)->where('id_empresa', $idEmpresa)->first();
                $presupuestoAnterior = Presupuesto::where('id_empresa', $idEmpresa)->orderBy('created_at', 'desc')->first();
            }
            else 
            {
                $cliente = Cliente::where('rut', $rutCliente)->where('eliminado', 0)->where('id_empresa', $user->getData()->id_empresa)->first();
                $presupuestoAnterior = Presupuesto::where('id_empresa', $user->getData()->id_empresa)->orderBy('created_at', 'desc')->first();
            }

            if (count($cliente) == 0)
            {
                Log::warning('Client not exists');
                return response()->json(['created' => false, 'error' => 'El cliente no existe.'], 412);
            }

            $presupuestoObject = new Presupuesto();

            $presupuestoObject->numero = count($presupuestoAnterior) > 0 ? $presupuestoAnterior->numero + 1 : 1;
            $presupuestoObject->cliente_direccion = $cliente->direccion;
            $presupuestoObject->cliente_email = $cliente->email;
            $presupuestoObject->cliente_nombre_contacto = $cliente->nombre_contacto;
            $presupuestoObject->cliente_razon_social = $cliente->razon_social;
            $presupuestoObject->cliente_rut = $cliente->rut;
            $presupuestoObject->cliente_telefono = $cliente->telefono;
            $presupuestoObject->cliente_giro = $cliente->giro;
            $presupuestoObject->fecha_expiracion = $fechaExpiracion;
            $presupuestoObject->nombre = $nombre;
            $presupuestoObject->email = $email;
            $presupuestoObject->asunto = $asunto;
            $presupuestoObject->descripcion = $descripcion;
            $presupuestoObject->id_empresa = $idEmpresa;
            $presupuestoObject->exenta = $exenta;
            $presupuestoObject->created_at = date('Y-m-d H:i:s');
            $presupuestoObject->updated_at = date('Y-m-d H:i:s');
            $presupuestoObject->save();

            if (!$this->addDetallePresupuesto($detallePresupuesto, $presupuestoObject->id, $exenta))
            {
                Log::warning('Presupuesto creado pero detalle presupuesto no.');
                return response()->json(['error' => 'Presupuesto creado, pero el detalle del presupuesto no ha podido ser guardado.', 'created' => false], 412);
            }
        }
        catch (\Exception $e)
        {
            Log::error($e);
            return response()->json(['created' => false], 500);
        }

        Log::info('Presupuesto created');
        return response()->json(['created' => true], 200);
    }

    public function addDetallePresupuesto($detalles, $id, $exenta)
    {
        try 
        {
            DetallePresupuesto::where('id_presupuesto', $id)->delete();

            foreach ($detalles as $detalle) 
            { 
                $detalleObject = new DetallePresupuesto();
                $detalleObject->id_presupuesto = $id;
                $detalleObject->iva = $exenta == 1 ? 0 : round($detalle['iva']);
                $detalleObject->unidades = $detalle['unidades'];
                $detalleObject->descripcion_articulo = $detalle['descripcion_articulo'];
                $detalleObject->precio = round($detalle['precio']);
                $detalleObject->codigo_articulo = $detalle['codigo_articulo'];
                $detalleObject->id_articulo = $detalle['id_articulo'];
                $detalleObject->save();
            }

            return true;
        } 
        catch (\Exception $e) 
        {
            Log::error($e);
            return false;
        }
    }

    public function getPresupuesto($id)
    {
        try
        {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
            
            if ($user->getStatusCode() != 200) 
            {
                Log::warning('Credenciales incorrectas');    
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if (!is_numeric($id))
            {
                Log::warning('ID is not number');
                return response()->json(['error' => 'El identificador del presupuesto no es numérico.'], 412);
            }

            $presupuesto = Presupuesto::where('id', $id)->where('eliminado', 0)->first();

            if ($user->getData()->id_rol != 1 && $presupuesto->id_empresa != $user->getData()->id_empresa)
            {
                Log::warning('El usuario no cuenta con permisos para obtener este presupuesto.');
                return response()->json(['error' => 'No cuenta con permisos para ver este presupuesto.'], 412);
            }

            $impuesto = Impuesto::where('nombre', 'iva')->first();

            $detalles = $presupuesto->detallesPresupuesto;

            for($i = 0; $i < count($detalles); $i++) 
            {

                $detalles[$i]->subtotal = $detalles[$i]->unidades * $detalles[$i]->precio;

                $presupuesto->sub_total += $detalles[$i]->subtotal;
            }
            

            $presupuesto->iva = $presupuesto->exenta == 1 ? 0 : number_format($presupuesto->sub_total * $impuesto->valor, 0, '', '');
            $presupuesto->total = $presupuesto->sub_total + $presupuesto->iva;

            $presupuesto->detalles_presupuesto = $detalles;
        }
        catch (\Exception $e)
        {
            Log::error($e);
            return response()->json(['created' => false, 'error' => 'Intentelo más tarde.'], 500);
        }

        return response()->json($presupuesto, 200);
    }

    
    public function update(PresupuestoRequest $request, $id)
    {
        $rutCliente = trim($request->input('cliente_rut'));
        $fechaExpiracion = date($request->input('fecha_expiracion'));
        $nombre = mb_strtolower(trim($request->input('nombre')), 'UTF-8');
        $email = mb_strtolower(trim($request->input('email')), 'UTF-8');
        $asunto = mb_strtolower(trim($request->input('asunto')), 'UTF-8');
        $descripcion = mb_strtolower(trim($request->input('descripcion')), 'UTF-8');
        $detallePresupuesto = $request->input('detalles_presupuesto');
        $exenta = $request->input('exenta');
    
        try
        {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
            
            if ($user->getStatusCode() != 200) 
            {
                Log::warning('Credenciales incorrectas');    
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            $idEmpresa = $user->getData()->id_empresa == 1 ? $request->input('id_empresa') : $user->getData()->id_empresa;

            if ($user->getData()->id_rol != 1) 
            {
                $cliente = Cliente::where('rut', $rutCliente)->where('eliminado', 0)->where('id_empresa', $idEmpresa)->first();
            }
            else 
            {
                $cliente = Cliente::where('rut', $rutCliente)->where('eliminado', 0)->where('id_empresa', $user->getData()->id_empresa)->first();
            }

            if (count($cliente) == 0)
            {
                Log::warning('Client not exists');
                return response()->json(['updated' => false, 'error' => 'El cliente no existe.'], 412);
            }

            if (!is_numeric($id))
            {
                Log::warning('El id del presupuesto no es válido');
                return response()->json(['updated' => false, 'error' => 'El presupuesto no es válido.'], 412);
            }

            $presupuestoObject = Presupuesto::find($id);

            $presupuestoObject->cliente_direccion = $cliente->direccion;
            $presupuestoObject->cliente_email = $cliente->email;
            $presupuestoObject->cliente_nombre_contacto = $cliente->nombre_contacto;
            $presupuestoObject->cliente_razon_social = $cliente->razon_social;
            $presupuestoObject->cliente_rut = $cliente->rut;
            $presupuestoObject->cliente_telefono = $cliente->telefono;
            $presupuestoObject->cliente_giro = $cliente->giro;
            $presupuestoObject->fecha_expiracion = $fechaExpiracion;
            $presupuestoObject->nombre = $nombre;
            $presupuestoObject->email = $email;
            $presupuestoObject->asunto = $asunto;
            $presupuestoObject->descripcion = $descripcion;
            $presupuestoObject->id_empresa = $user->getData()->id_empresa == 1 ? $request->input('id_empresa') : $user->getData()->id_empresa;
            $presupuestoObject->updated_at = date('Y-m-d H:i:s');
            $presupuestoObject->exenta = $exenta;
            $presupuestoObject->save();

            if (!$this->addDetallePresupuesto($detallePresupuesto, $presupuestoObject->id, $exenta))
            {
                Log::warning('Presupuesto creado pero detalle presupuesto no.');
                return response()->json(['error' => 'Presupuesto creado, pero el detalle del presupuesto no ha podido ser guardado.', 'updated' => false], 204);
            }
        }
        catch (\Exception $e)
        {
            Log::error($e);
            return response()->json(['updated' => false], 500);
        }

        Log::info('Presupuesto created');
        return response()->json(['updated' => true], 200);
    }

    public function destroy($id)
    {
        try
        {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
            
            if ($user->getStatusCode() != 200) 
            {
                Log::warning('Credenciales incorrectas');    
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            $presupuesto = Presupuesto::find($id);

            if (count($presupuesto) == 0)
            {
                Log::info('Presupuesto no existe');
                return response()->json(['error' => 'El presupuesto no existe.'], 400);
            }

            $presupuesto->eliminado = 1;
            $presupuesto->save();
        }
        catch(\Exception $e)
        {
            Log:error($e);
            return response()->json(['error' => 'Itentelo más tarde.'], 500);
        }

        return response()->json(['deleted' => true], 200);
    }


    public function getDefaultEmailPresupuesto($id)
    {
        try
        {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
            
            if ($user->getStatusCode() != 200) 
            {
                Log::warning('Credenciales incorrectas');    
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if ($user->getData()->id_rol == 1)
            {
                $presupuesto = Presupuesto::where('id', $id)->where('eliminado', 0)->first();
            }
            else 
            {
                $presupuesto = Presupuesto::where('id', $id)->where('eliminado', 0)->where('id_empresa', $user->getData()->id_empresa)->first();                
            }

            if (count($presupuesto) == 0)
            {
                Log::warning('No existe el presupuesto');
                return response()->json(['error' => 'No existe el presupuesto'], 400);
            }

            $cliente = Cliente::where('rut', $presupuesto->cliente_rut)->where('id_empresa', $presupuesto->id_empresa)->where('eliminado', 0)->first();

            if (count($cliente) == 0)
            {
                Log::warning('No existe el cliente');
                return response()->json(['error' => 'No existe el cliente'], 400);
            }

            return response()->json($cliente->email, 200);
        }
        catch (\Exception $e)
        {
            Log::error($e);
            return response()->json(['error' => 'Intentelo más tarde.'], 500);
        }
    }

    public function sendPresupuestoEmail(Request $request, $id)
    {
        try
        {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
            
            if ($user->getStatusCode() != 200) 
            {
                Log::warning('Credenciales incorrectas');    
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if (!is_numeric($id))
            {
                Log::warning('El ID del presupuesto no es numérico');
                return response()->json(['error' => 'El ID del presupuesto no es numérico.'], 400);
            }

            if (empty($request->input('email'))) 
            {
                Log::warning('El correo es vacío');
                return response()->json(['error' => 'Debe ingresar un email.'], 400);
            }

            if ($user->getData()->id_rol == 1)
            {
                $presupuesto = Presupuesto::where('id', $id)->where('eliminado', 0)->first();
            }
            else 
            {
                $presupuesto = Presupuesto::where('id', $id)->where('eliminado', 0)->where('id_empresa', $user->getData()->id_empresa)->first();                
            }

            if (count($presupuesto) == 0)
            {
                Log::warning('No existe el presupuesto');
                return response()->json(['error' => 'No existe el presupuesto'], 400);
            }

            $impuesto = Impuesto::where('nombre', 'iva')->first();
            
            $makeReporte = new MakeReporte();
            $reporte = $makeReporte->makeReporteDetallePresupuesto($presupuesto, 'Presupuesto', $user->getData(), $impuesto->valor);

            $fullPath = 'documents/' . $reporte . '.pdf';
        }
        catch (\Exception $e)
        {
            Log::error($e);
            return response()->json(['error' => 'Intentelo más tarde.'], 500);
        }

        $mail = new PHPMailer(true);

        try{

            $presupuestoTemplate = new PresupuestoTemplateEmail();

            $mail->isSMTP();
            $mail->CharSet = 'UTF-8';
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = 'ssl';
            $mail->Host = APP_MAIL_HOST;
            $mail->Port = 465;
            $mail->Username = APP_MAIL_USERNAME;
            $mail->Password = APP_MAIL_PASSWORD;
            $mail->setFrom(APP_MAIL_FROM, $presupuesto->empresa->razon_social); 
            $mail->Subject = $presupuesto->asunto;
            $mail->isHTML(true);
            $mail->MsgHtml($presupuestoTemplate->presupuestoTemplate($presupuesto));
            $mail->addAddress($request->input('email'), $presupuesto->cliente_razon_social); 
            $mail->addAttachment($fullPath, 'presupuesto.pdf');  

            $mail->send();

            if (file_exists($fullPath))
            {
                unlink($fullPath);
            }

            return response()->json(['status' => true], 200);
        } 
        catch(\Exception $e)
        {
            Log::error($e);
            return response()->json(['error' => 'Intentelo más tarde.'], 500);
        } 
    }

}
