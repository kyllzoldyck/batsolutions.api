<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Presupuesto extends Model
{
    protected $table = 'presupuestos';

    protected $fillable = [
        'cliente_direccion',
        'cliente_email',
        'cliente_nombre_contacto',
        'cliente_razon_social',
        'cliente_rut',
        'cliente_telefono',
        'cliente_giro',
        'fecha_expiracion',
        'nombre',
        'email',
        'asunto',
        'descripcion',
        'id_empresa',
        'eliminado',
        'numero',
        'exenta'
    ];

    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $connection = '';

    public function empresa() {
        return $this->belongsTo('App\Entity', 'id_empresa');
    }

    public function detallesPresupuesto() {
        return $this->hasMany('App\DetallePresupuesto', 'id_presupuesto', 'id');
    }
}
