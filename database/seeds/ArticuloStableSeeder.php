<?php

use Illuminate\Database\Seeder;

class ArticuloTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Articulo::class, 100)->create();


        // DB::table('articulos')->insert([
        //     [
        //         'descripcion' => 'coca cola 1L',
        //         'eliminado' => 0,
        //         'id_proveedor' => 1,
        //         'precio_de_compra' => 600,
        //         'precio_de_venta' => 1000,
        //         'stock' => 100,
        //         'codigo' => 'C100'
        //     ],
        //     [
        //         'descripcion' => 'sprite 1L',
        //         'eliminado' => 0,
        //         'id_proveedor' => 1,
        //         'precio_de_compra' => 650,
        //         'precio_de_venta' => 1100,
        //         'stock' => 100,
        //         'codigo' => 'S100'
        //     ],
        //     [
        //         'descripcion' => 'chocolate costa',
        //         'eliminado' => 0,
        //         'id_proveedor' => 2,
        //         'precio_de_compra' => 1100,
        //         'precio_de_venta' => 1500,
        //         'stock' => 100,
        //         'codigo' => 'CH100'
        //     ],
        //     [
        //         'descripcion' => 'chocolate sahne nuss',
        //         'eliminado' => 0,
        //         'id_proveedor' => 2,
        //         'precio_de_compra' => 550,
        //         'precio_de_venta' => 900,
        //         'stock' => 100,
        //         'codigo' => 'CHSN100'
        //     ]
        // ]);
    }
}
