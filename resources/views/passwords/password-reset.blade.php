<div>
    <h5>Restablecer contraseña</h5>
    <p>
        Estimado <strong>{{ $user->nombre }}</strong>. <a href="http://localhost:4200/auth/password-reset?t={{ $token }}">Haz click aquí para restablecer tu contraseña.</a>
        <br>
        <br>
        <table>
            <tbody>
                <tr>
                    <td colspan="2">Datos del usuario</td>
                </tr>
                <tr>
                    <td><strong>Nombre de usuario: </strong></td>
                    <td>{{ $user->username }}</td>
                </tr>
                <tr>
                    <td><strong>Empresa:</strong></td>
                    <td>{{ $user->empresa->razon_social }}</td>
                </tr>
                <tr>
                    <td><strong>Estado:</strong></td>
                    <td>{{ $user->activo == 1 ? 'Activo' : 'Inactivo' }}</td>
                </tr>
            </tbody>
        </table>

    </p>
    <br>
    <strong>Si recibiste este correo por error, por favor bórralo.</strong>
    <br>
    <strong>Atentamente BAT Solution SPA.</strong>
</div>