<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\User;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = User::find($this->usuario);

        $rules = [
            'apellidos' => 'required|max:150',
            'email' => 'required|unique:usuarios|max:100',
            'activo' => 'numeric',
            'id_rol' => 'required|numeric',
            'eliminado' => 'numeric',
            'nombre' => 'required|max:100',
            'password' => 'required|max:255',
            'username' => 'required|max:10|unique:usuarios',
            'id_empresa' => 'required|numeric'
        ];

        switch ($this->method()) {
            case 'PUT':
                $rules = [
                    'apellidos' => 'required|max:150',
                    'email' => 'required|unique:usuarios,email,'.$user->id.'|max:100',
                    'activo' => 'numeric',
                    'id_rol' => 'required|numeric',
                    'nombre' => 'required|max:100',
                    'username' => 'required|max:10|unique:usuarios,username,'.$user->id,
                    'id_empresa' => 'required|numeric'
                ];
                break;
            default:
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'apellidos.required' => 'El campo apellido es obligatorio.',
            'apellidos.max:150' => 'El campo apellido supera el máximo de caracteres.',

            'email.required' => 'El campo email es obligatorio.',
            'email.unique' => 'El email ya existe.',
            'email.max:100' => 'El campo email supera el máximo de caracteres.',

            'activo.numeric' => 'El valor activo no es correcto.',

            'id_rol.required' => 'El valor rol es obligatorio.',
            'id_rol.numeric' => 'El valor rol no es correcto.',

            'eliminado' => 'El valor eliminado no es correcto.',

            'nombre.required' => 'El campo nombre es obligatorio.',
            'nombre.max:100' => 'El campo nombre supera el máximo de careceteres.',

            'password.required' => 'El campo password es obligatorio.',
            'password.max:255' => 'El campo password supera el máximo de careceteres.',

            'username.required' => 'El campo usuario es obligatorio.',
            'username.max:10' => 'El campo usuario supera el máximo de caracteres.',
            'username.unique' => 'El usuario ya existe en el sistema',

            'id_empresa.required' => 'El campo empresa es obligatorio.',
            'id_empresa.numeric' => 'El valor empresa no es correcto.'
        ];
    }
}
