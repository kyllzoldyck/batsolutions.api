<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ResetPasswordRequest;
use Illuminate\Support\Facades\Log;
use DB;
use App\User;

class ResetPasswordController extends Controller
{
    public function reset(ResetPasswordRequest $request)
    {
        try
        {
            $token = $request->input('token');
            $password = $request->input('password');

            $passwordReset = DB::table('password_resets')->where('token', $token)->first();

            if (count($passwordReset) == 0)
            {
                return response()->json(['error' => 'El token de autorización no es válido.'], 400);
            }

            $user = User::where('email', $passwordReset->email)->where('activo', 1)->first();
            
            if (count($user) == 0)
            {
                return response()->json(['error' => 'El usuario no existe o se encuentra deshabilitado. Pongase en contacto con el adminsitrador.'], 400);
            }

            $user->password = bcrypt($password);
            $user->save();

            DB::table('password_resets')->where('email', $passwordReset->email)->delete();

            return response()->json(['status' => true], 200);
        }
        catch (\Exception $e)
        {
            dd($e);
            Log::error($e);
            return response()->json(['error' => 'Intentelo más tarde.'], 500);
        }
    }
}
