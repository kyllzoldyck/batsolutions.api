<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotaDeCreditoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nota_de_credito', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->integer('eliminado');
            $table->dateTime('fecha');
            $table->dateTime('fecha_de_creacion');
            $table->integer('id_factura')->unsigned();
            $table->integer('numero');

            $table->foreign('id_factura')->references('id')->on('facturas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nota_de_credito');
    }
}
