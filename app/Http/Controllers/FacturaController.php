<?php

namespace App\Http\Controllers;

use App\Http\Requests\FacturaRequest;
use App\Http\Controllers\Api\AuthController;
use DB;
use App\Factura;
use App\DetalleFactura;
use App\Proveedor;
use App\Articulo;
use App\Cliente;
use Illuminate\Support\Facades\Log;

class FacturaController extends Controller
{
    public function index()
    {
        try {
            Log::info('Hi, getting factura');

            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                Log::warning('Invalid credentials');
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            $facturas = [];

            if ($user->getData()->id_rol == 1) {
                $facturas = Factura::where('eliminado', 0)->orderBy('fecha_de_creacion', 'desc')->get();
            }
            else {
                $facturas = Factura::where('eliminado', 0)->where('id_empresa', $user->getData()->id_empresa)->orderBy('fecha_de_creacion', 'desc')->get();
            }

            $facturas->each(function (Factura $factura) {
                $factura->empresa;
            });

            Log::info('Facturas obtained');
            return response()->json($facturas, 200);

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json($e, 500);
        }
    }

    public function store(FacturaRequest $request)
    {
        try {

            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if ($user->getData()->id_rol == 1 && !is_numeric($request->input('id_empresa'))) {
                return response()->json(['error' => 'La identificación de la empresa debe ser numérico.'], 404);
            }

            switch ($request->input('tipo_factura')) {
                # Venta
                case 1:
                    if ($user->getData()->id_rol == 1) {
                        $entity = Cliente::where('rut', $request->input('rut'))
                            ->where('eliminado', 0)
                            ->where('id_empresa', $request->input('id_empresa'))
                            ->first();
                    } else {
                        $entity = Cliente::where('rut', $request->input('rut'))
                            ->where('eliminado', 0)
                            ->where('id_empresa', $user->getData()->id_empresa)
                            ->first();
                    }

                    break;
                # Compra
                case 2:
                    if ($user->getData()->id_rol == 1) {
                        $entity = Proveedor::where('rut', $request->input('rut'))
                            ->where('eliminado', 0)
                            ->where('id_empresa', $request->input('id_empresa'))
                            ->first();
                    } else {
                        $entity = Proveedor::where('rut', $request->input('rut'))
                            ->where('eliminado', 0)
                            ->where('id_empresa', $user->getData()->id_empresa)
                            ->first();
                    }
                    break;
                default:
                    return response()->json(['error' => 'El tipo de factura no es el correcto.'], 404);
                    break;
            }

            if (count($entity) == 0) {
                return response()->json(['error' => 'No existe una entidad para asociar a la factura.'], 404);
            }

            if (! $this->validarNumeroFactura($request->input('numero'), $request->input('tipo_factura'), $request->input('rut'), $request->input('exenta'))) {
                return response()->json(['error' => 'Ya existe una factura con este número.'], 404);
            }
            $exenta = $request->input('exenta');
            
            $factura = new Factura();
            $factura->descripcion = $request->input('descripcion');
            // $factura->email = $request->input('email');
            $factura->nula = 0;
            $factura->eliminado = 0;
            $factura->fecha = \DateTime::createFromFormat('Y-m-d', $request->input('fecha'))->format('Y-m-d H:i:s');
            $factura->fecha_de_creacion = date('Y-m-d H:i:s');
            $factura->iva = $exenta == 1 ? 0 : round($request->input('iva'));
            $factura->numero = $request->input('numero');
            $factura->tipo_factura = $request->input('tipo_factura');
            $factura->razon_social = $entity->razon_social;
            $factura->direccion = $entity->direccion;
            $factura->rut = $request->input('rut');
            // $factura->telefono = $request->input('telefono');
            $factura->nombre_contacto = $entity->nombre_contacto;
            $factura->id_empresa = $entity->id_empresa;
            $factura->giro = $entity->giro;
            $factura->exenta = $exenta;
            $factura->save();

            if (! $this->addDetalleFactura($request->input('detalles_factura'), $factura->id, $request->input('tipo_factura'), $exenta)) {
                return response()->json(['error' => 'Ha ocurrido un error al intentar guardar el detalle de la factura.'], 400);
            }

            $factura = Factura::find($factura->id);
            $factura->detallesFactura;

            return response()->json($factura, 200);

        } catch (\Exception $e) {
            return response()->json($e, 500);
        }
    }

    public function show($id)
    {
        try {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }
            if (!is_numeric($id)) {
                return response()->json(['error' => 'El ID debe ser numérico'], 404);
            }

            $factura = Factura::find($id);
            $factura->detallesFactura;

            if ($factura->eliminado == 1) {
                return response()->json(['error' => 'La factura está eliminada.'], 404);
            }

            for ($i = 0; $i < count($factura->detallesFactura); $i++) {
                $factura->sub_total += $factura->detallesFactura[$i]->precio * $factura->detallesFactura[$i]->unidades;
                $factura->total += $factura->exenta == 1 ? ($factura->detallesFactura[$i]->precio * $factura->detallesFactura[$i]->unidades) : ($factura->detallesFactura[$i]->precio * $factura->detallesFactura[$i]->unidades) + $factura->detallesFactura[$i]->iva;
            }

            if ($user->getData()->id_rol == 1) {
                return response()->json($factura, 200);
            }

            if ($user->getData()->id_empresa !== $factura->id_empresa) {
                return response()->json(['error' => 'No cuenta con permisos para leer esta factura.'], 404);
            }

            return response()->json($factura, 200);

        } catch (\Exception $e) {
            return response()->json($e, 500);
        }
    }

    public function update(FacturaRequest $request, $id)
    {
        try {

            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if (! is_numeric($id)) {
                return response()->json(['error' => 'El valor de ID es incorrecto.'], 404);
            }

            if ($user->getData()->id_rol == 1 && !is_numeric($request->input('id_empresa'))) {
                return response()->json(['error' => 'La identificación de la empresa debe ser numérico.'], 404);
            }

            $factura = Factura::where('id', $id)
                ->where('eliminado', 0)
                ->first();

            switch ($factura->tipo_factura) {
                # Venta
                case 1:
                    if ($user->getData()->id_rol == 1) {
                        $entity = Cliente::where('rut', $request->input('rut'))
                            ->where('eliminado', 0)
                            ->where('id_empresa', $request->input('id_empresa'))
                            ->first();
                    } else {
                        $entity = Cliente::where('rut', $request->input('rut'))
                            ->where('eliminado', 0)
                            ->where('id_empresa', $user->getData()->id_empresa)
                            ->first();
                    }

                    break;
                # Compra
                case 2:
                    if ($user->getData()->id_rol == 1) {
                        $entity = Proveedor::where('rut', $request->input('rut'))
                            ->where('eliminado', 0)
                            ->where('id_empresa', $request->input('id_empresa'))
                            ->first();
                    } else {
                        $entity = Proveedor::where('rut', $request->input('rut'))
                            ->where('eliminado', 0)
                            ->where('id_empresa', $user->getData()->id_empresa)
                            ->first();
                    }
                    break;
                    
                default:
                    return response()->json(['error' => 'El tipo de factura no es el correcto.'], 404);
                    break;
            }

            if (count($entity) == 0) {
                return response()->json(['error' => 'No existe una entidad para asociar a la factura.'], 404);
            }

            if (count($factura) == 0) {
                return response()->json(['error' => 'La factura que esta intentando actualizar no existe.'], 404);
            }

            $factura->descripcion = $request->input('descripcion');
            // $factura->email = $request->input('email');
            $factura->fecha = date($request->input('fecha'));
            $factura->razon_social = $entity->razon_social;
            $factura->direccion = $entity->direccion;
            $factura->rut = $entity->rut;
            // $factura->telefono = $request->input('telefono');
            $factura->nombre_contacto = $entity->nombre_contacto;
            $factura->id_empresa = $entity->id_empresa;
            $factura->giro = $entity->giro;
            $factura->save();

            $factura = Factura::find($factura->id);
            $factura->detallesFactura;

            return response()->json($factura, 200);

        } catch (\Exception $e) {
            return response()->json($e, 500);
        }
    }
    
    public function destroy($id)
    {
        try {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 401);
            }

            if (! is_numeric($id)) {
                return response()->json(['error' => 'El ID no es numérico.'], 400);
            }

            if ($user->getData()->id_rol == 1) {
                $factura = Factura::where('id', $id)->first();
            }
            else {
                $factura = Factura::where('id', $id)->where('id_empresa', $user->getData()->id_empresa)->first();
            }
            
            if (count($factura) == 0) {
                return response()->json(['error' => 'No se ha encontrado una factura para eliminar.'], 400);
            }

            if ($factura->nula == 0)
            {
                foreach($factura->detallesFactura as $detalle)
                {
                    $articulo = Articulo::find($detalle->id_articulo);

                    if (count($articulo) > 0)
                    {
                        # Venta
                        if ($factura->tipo_factura == 1) 
                        {
                            $articulo->stock = $articulo->stock + $detalle->unidades;
                        }
                        # Compra
                        else 
                        {
                            $articulo->stock = $articulo->stock - $detalle->unidades;
                        }

                        $articulo->save();
                    }
                }  
            }

            $factura->eliminado = 1;
            $factura->save();

            return response()->json(['delete' => true, 'message' => 'Factura eliminada'], 204);

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['error' => 'Intentelo más tarde.'], 500);
        }
    }
    
    public function getFacturasByNumero($numero)
    {
        try {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if (! is_numeric($numero)) {
                return response()->json(['error' => 'El número de la factura no es correcto.'], 404);
            }

            $facturas = [];

            if ($user->getData()->id_rol == 1) {
                $facturas = DB::select('SELECT f.id, f.descripcion, f.email, f.nula, f.eliminado, f.fecha, f.fecha_de_creacion, f.iva, f.numero, f.tipo_factura, f.razon_social, f.direccion, f.rut, f.telefono, f.nombre_contacto, f.id_empresa, ROUND(SUM((df.precio * df.unidades) + df.iva), 0) AS total, SUM(df.unidades * df.precio) AS subTotal
                    FROM facturas f, detalle_factura df
                    WHERE f.id = df.id_factura AND f.eliminado = 0 AND f.numero = ?
                    GROUP BY f.id ORDER BY f.fecha_de_creacion DESC', [$numero]);
            }
            else {
                $facturas = DB::select('SELECT f.id, f.descripcion, f.email, f.nula, f.eliminado, f.fecha, f.fecha_de_creacion, f.iva, f.numero, f.tipo_factura, f.razon_social, f.direccion, f.rut, f.telefono, f.nombre_contacto, f.id_empresa, ROUND(SUM((df.precio * df.unidades) + df.iva), 0) AS total, SUM(df.unidades * df.precio) AS subTotal
                    FROM facturas f, detalle_factura df
                    WHERE f.id = df.id_factura AND f.eliminado = 0 AND f.numero = ? AND f.id_empresa = ?
                    GROUP BY f.id ORDER BY f.fecha_de_creacion DESC', [$numero, $user->getData()->id_empresa]);
            }

            return response()->json($facturas, 200);
        } catch (\Exception $e) {
            return response()->json($e, 500);
        }
    }

    public function getFacturasCompra()
    {
        try {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if ($user->getData()->id_rol == 1) 
            {
                $facturas = DB::select(
                    'SELECT 
                        f.id, 
                        f.descripcion, 
                        f.email, 
                        f.nula, 
                        f.eliminado, 
                        f.fecha, 
                        f.fecha_de_creacion, 
                        f.iva, 
                        f.numero, 
                        f.tipo_factura, 
                        f.razon_social, 
                        f.direccion, 
                        f.rut, 
                        f.telefono, 
                        f.nombre_contacto, 
                        f.id_empresa, 
                        (SELECT ROUND(SUM((detalle_factura.precio * detalle_factura.unidades) + detalle_factura.iva), 0) AS total FROM detalle_factura WHERE detalle_factura.id_factura = f.id) AS total, 
                        SUM(df.unidades * df.precio) AS subTotal, 
                        e.razon_social as name_empresa,
                        f.exenta,
                        f.giro
                    FROM 
                        facturas f, 
                        detalle_factura df, 
                        entities e
                    WHERE 
                        f.id = df.id_factura AND 
                        f.eliminado = 0 AND 
                        f.tipo_factura = 2 AND
                        e.id = f.id_empresa 
                    GROUP BY 
                        f.id 
                    ORDER BY 
                        f.fecha_de_creacion DESC');
            } 
            else 
            {
                $facturas = DB::select(
                    'SELECT 
                        f.id, 
                        f.descripcion, 
                        f.email, 
                        f.nula, 
                        f.eliminado, 
                        f.fecha, 
                        f.fecha_de_creacion, 
                        f.iva, 
                        f.numero, 
                        f.tipo_factura, 
                        f.razon_social, 
                        f.direccion, 
                        f.rut, 
                        f.telefono, 
                        f.nombre_contacto, 
                        f.id_empresa, 
                        (SELECT ROUND(SUM((detalle_factura.precio * detalle_factura.unidades) + detalle_factura.iva), 0) AS total FROM detalle_factura WHERE detalle_factura.id_factura = f.id) AS total, 
                        SUM(df.unidades * df.precio) AS subTotal,
                        e.razon_social as name_empresa,
                        f.exenta,
                        f.giro
                    FROM 
                        facturas f, 
                        detalle_factura df,
                        entities e
                    WHERE 
                        f.id = df.id_factura AND 
                        f.eliminado = 0 AND 
                        f.tipo_factura = 2 AND 
                        e.id = f.id_empresa AND
                        f.id_empresa = ? 
                    GROUP BY 
                        f.id 
                    ORDER BY 
                        f.fecha_de_creacion DESC', 
                    [$user->getData()->id_empresa]);
            }

            return response()->json($facturas, 200);

        } 
        catch (\Exception $e) {    
            return response()->json($e, 500);
        }
    }
    
    public function getFacturaCompraByNumero($numero)
    {
        try {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if (!is_numeric($numero)) {
                return response()->json(['error' => 'El número de factura es incorrecto.'], 404);
            }
            $facturas = [];
            if ($user->getData()->id_rol == 1) {
                $facturas = DB::select('SELECT f.id, f.descripcion, f.email, f.nula, f.eliminado, f.fecha, f.fecha_de_creacion, f.iva, f.numero, f.tipo_factura, f.razon_social, f.direccion, f.rut, f.telefono, f.nombre_contacto, f.id_empresa, ROUND(SUM((df.precio * df.unidades) + df.iva), 0) AS total, SUM(df.unidades * df.precio) AS subTotal
                    FROM facturas f, detalle_factura df
                    WHERE f.id = df.id_factura AND f.eliminado = 0 AND f.numero = ? AND f.tipo_factura = 2
                    GROUP BY f.id ORDER BY f.fecha_de_creacion DESC', [$numero]);
            } else {
                $facturas = DB::select('SELECT f.id, f.descripcion, f.email, f.nula, f.eliminado, f.fecha, f.fecha_de_creacion, f.iva, f.numero, f.tipo_factura, f.razon_social, f.direccion, f.rut, f.telefono, f.nombre_contacto, f.id_empresa, ROUND(SUM((df.precio * df.unidades) + df.iva), 0) AS total, SUM(df.unidades * df.precio) AS subTotal
                    FROM facturas f, detalle_factura df
                    WHERE f.id = df.id_factura AND f.eliminado = 0 AND f.numero = ? AND f.id_empresa = ? AND f.tipo_factura = 2
                    GROUP BY f.id ORDER BY f.fecha_de_creacion DESC', [$numero, $user->getData()->id_empresa]);
            }
            $facturas = json_encode($facturas);
            return response()->json($facturas, 200);
        } catch (\Exception $e) {

        }
    }

    public function getFacturasVenta()
    {
        try {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if ($user->getData()->id_rol == 1) 
            {
                $facturas = DB::select(
                    'SELECT 
                        f.id, 
                        f.descripcion, 
                        f.email, 
                        f.nula, 
                        f.eliminado, 
                        f.fecha, 
                        f.fecha_de_creacion, 
                        f.iva, 
                        f.numero, 
                        f.tipo_factura, 
                        f.razon_social, 
                        f.direccion, 
                        f.rut, 
                        f.telefono, 
                        f.nombre_contacto, 
                        f.id_empresa, 
                        (SELECT ROUND(SUM((detalle_factura.precio * detalle_factura.unidades) + detalle_factura.iva), 0) AS total FROM detalle_factura WHERE detalle_factura.id_factura = f.id) AS total, 
                        (df.unidades * df.precio) AS subTotal,
                        e.razon_social as name_empresa,
                        f.exenta,
                        f.giro
                    FROM 
                        facturas f, 
                        detalle_factura df,
                        entities e
                    WHERE 
                        f.id = df.id_factura AND 
                        f.eliminado = 0 AND 
                        e.id = f.id_empresa AND
                        f.tipo_factura = 1
                    GROUP BY 
                        f.id
                    ORDER BY 
                        f.fecha_de_creacion DESC');
            } 
            else 
            {
                $facturas = DB::select(
                    'SELECT 
                        f.id, 
                        f.descripcion, 
                        f.email, 
                        f.nula, 
                        f.eliminado, 
                        f.fecha, 
                        f.fecha_de_creacion, 
                        f.iva, 
                        f.numero, 
                        f.tipo_factura, 
                        f.razon_social, 
                        f.direccion, 
                        f.rut, 
                        f.telefono, 
                        f.nombre_contacto, 
                        f.id_empresa, 
                        (SELECT ROUND(SUM((detalle_factura.precio * detalle_factura.unidades) + detalle_factura.iva), 0) AS total FROM detalle_factura WHERE detalle_factura.id_factura = f.id) AS total, 
                        (df.unidades * df.precio) AS subTotal,
                        e.razon_social as name_empresa,
                        f.exenta,
                        f.giro
                    FROM 
                        facturas f, 
                        detalle_factura df,
                        entities e
                    WHERE 
                        f.id = df.id_factura AND 
                        f.eliminado = 0 AND 
                        f.tipo_factura = 1 AND 
                        e.id = f.id_empresa AND
                        f.id_empresa = ?
                    GROUP BY 
                        f.id
                    ORDER BY 
                        f.fecha_de_creacion DESC', 
                    [$user->getData()->id_empresa]);
            }

            return response()->json($facturas, 200);

        } 
        catch (\Exception $e) {    
            Log::error($e);
            return response()->json($e, 500);
        }
    }

    public function getFacturaVentaByNumero($numero)
    {
        try {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if (!is_numeric($numero)) {
                return response()->json(['error' => 'El número de factura es incorrecto.'], 404);
            }
            $facturas = [];
            if ($user->getData()->id_rol == 1) {
                $facturas = DB::select('SELECT f.id, f.descripcion, f.email, f.nula, f.eliminado, f.fecha, f.fecha_de_creacion, f.iva, f.numero, f.tipo_factura, f.razon_social, f.direccion, f.rut, f.telefono, f.nombre_contacto, f.id_empresa, ROUND(SUM((df.precio * df.unidades) + df.iva), 0) AS total, SUM(df.unidades * df.precio) AS subTotal
                    FROM facturas f, detalle_factura df
                    WHERE f.id = df.id_factura AND f.eliminado = 0 AND f.numero = ? AND f.tipo_factura = 1
                    GROUP BY f.id ORDER BY f.fecha_de_creacion DESC', [$numero]);
            } else {
                $facturas = DB::select('SELECT f.id, f.descripcion, f.email, f.nula, f.eliminado, f.fecha, f.fecha_de_creacion, f.iva, f.numero, f.tipo_factura, f.razon_social, f.direccion, f.rut, f.telefono, f.nombre_contacto, f.id_empresa, ROUND(SUM((df.precio * df.unidades) + df.iva), 0) AS total, SUM(df.unidades * df.precio) AS subTotal
                    FROM facturas f, detalle_factura df
                    WHERE f.id = df.id_factura AND f.eliminado = 0 AND f.numero = ? AND f.id_empresa = ? AND f.tipo_factura = 1
                    GROUP BY f.id ORDER BY f.fecha_de_creacion DESC', [$numero, $user->getData()->id_empresa]);
            }
            $facturas = json_encode($facturas);
            return response()->json($facturas, 200);
        } catch (\Exception $e) {

        }
    }

    public function validarNumeroFactura($numero, $tipo_factura, $rut_entity, $exenta)
    {
        try {
            $factura = Factura::where('numero', $numero)
                ->where('rut', $rut_entity)
                ->where('tipo_factura', $tipo_factura)
                ->where('exenta', $exenta)
                ->where('eliminado', 0)
                ->get();
                
            if (count($factura) > 0) {
                return false;
            }
            return true;

        } catch (\Exception $e) {
            return false;
        }
    }

    public function addDetalleFactura($detalle_factura_array, $id, $tipo_factura, $exenta)
    {
        try {
            foreach ($detalle_factura_array as $detalle) {

                $detalle_factura = new DetalleFactura();
                $detalle_factura->id_factura = $id;
                $detalle_factura->iva = $exenta == 1 ? 0 : round($detalle['iva']);
                $detalle_factura->unidades = $detalle['unidades'];
                $detalle_factura->descripcion_articulo = $detalle['descripcion_articulo'];
                $detalle_factura->precio = round($detalle['precio']);
                $detalle_factura->codigo_articulo = $detalle['codigo_articulo'];
                $detalle_factura->id_articulo = $detalle['id_articulo'];
                $detalle_factura->save();

                $articulo = Articulo::where('id', $detalle['id_articulo'])
                    ->where('eliminado', 0)
                    ->first();

                if (count($articulo) == 0) {
                    return false;
                    // return response()->json(['error' => 'El articulo no existe o esta eliminado.'], 404);
                } 
                else if ($detalle['unidades'] > $articulo->stock && $tipo_factura == 1) {
                    return false;
                    // return response()->json(['error' => 'No cuenta con stock de ' . $detalle['descripcion_articulo'] . '.'], 404);
                }

                $articulo->stock = $this->calcularStock($tipo_factura, $articulo->stock, $detalle['unidades']);
                $articulo->save();
            }
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function calcularStock($tipo_factura, $stock, $unidades)
    {        
        if ($tipo_factura == 1) {
            $stock -= $unidades;
        } else if ($tipo_factura == 2) {
            $stock += $unidades;
        }
        return $stock;
    }
}
