<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Impuesto;
use App\Http\Controllers\Api\AuthController;

class ImpuestoController extends Controller
{
    public function index()
    {
        try {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            $impuestos = Impuesto::where('state', 1)->get();
            return response()->json($impuestos, 200);
        } catch (\Exception $e) {
            return response()->json($e, 500);
        }
    }

    public function show($id)
    {
        try {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            $impuesto = Impuesto::where('state', 1)->where('id', $id)->get();
            return response()->json($impuesto, 200);

        } catch (\Exception $e) {
            return response()->json($e, 500);
        }
    }

    public function getImpuestoByNombre($nombre)
    {
        try {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if (empty($nombre))
                return response()->json(['error' => 'El nombre no debe ser vacío.'], 404);

            $impuesto = Impuesto::where('nombre', $nombre)->where('state', 1)->first();
            return response()->json($impuesto, 200);
            
        } catch (\Exception $e) {
            return response()->json($e, 500);
        }
    }
}
