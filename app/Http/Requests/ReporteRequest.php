<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReporteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fecha_desde' => 'required|date',
            'fecha_hasta' => 'required|date',
            'id_empresa' => 'required|numeric'
        ];
    }
    
    public function messages()
    {
        return [
            'fecha_desde.required' => 'La fecha desde es obligatoria.',
            'fecha_desde.date' => 'La fecha desde no cumple el formato correcto.',

            'fecha_hasta.required' => 'La fecha hasta es obligatoria.',
            'fecha_hasta.date' => 'La fecha hasta no cumple el formato correcto.',

            'id_empresa.required' => 'La identificación de la empresa es obligatoria.',
            'id_empresa.numeric' => 'El ID de la empresa debe ser numerico'
        ];
    }
}
