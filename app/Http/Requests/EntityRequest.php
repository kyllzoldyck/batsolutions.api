<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EntityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:100|min:5',
            'razon_social' => 'required|max:150|min:5',
            'url_img' => 'required|max:100',
            'state' => 'required|numeric|boolean',
            'rut' => 'required|cl_rut|max:12'
        ];

        switch ($this->method())
        {
            case 'DELETE':
                $rules = [
                    'id' => 'required|numeric'
                ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'id.required' => 'El id de la entidad es obligatorio.',
            'id.numeric' => 'El id de la entidad debe ser numérico.',

            'name.required' => 'El nombre de la entidad es obligatorio.',
            'name.max:100' => 'El nombre de la entidad debe tener un máximo de 100 caracteres.',
            'name.min:5' => 'El nombre de la entidad debe tener un minimo de 5 caracteres.',

            'razon_social.required' => 'La razon social es obligatoria.',
            'razon_social.max:150' => 'La razon social debe tener un máximo de 150 caracteres.',
            'razon_social.min:5' => 'La razon social debe tener un minimo de 5 caracteres.',

            'url_img.required' => 'La ruta de la imagen es obligatoria.', 
            'url_img.max:150' => 'La ruta debe tener un máximo de 100 caracteres.',

            'state.required' => 'El estado del menu es obligatorio.',
            'state.numeric' => 'El estado es de tipo numérico.',
            'state.boolean' => 'El estado debe ser 1 o 0.',

            'rut.required' => 'El rut es obligatorio.',
            'rut.cl_rut' => 'El rut es inválido.',
            'rut.max:12' => 'El rut debe tener un máximo de 12 caracteres.'
        ];
    }
}
