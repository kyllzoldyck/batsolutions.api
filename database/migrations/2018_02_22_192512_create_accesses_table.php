<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accesses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_print_menu')->unsigned();
            $table->integer('id_entity')->unsigned();
            $table->integer('id_rol')->unsigned();
            $table->timestamps();
            $table->boolean('state');

            $table->foreign('id_print_menu')->references('id')->on('print_menus');
            $table->foreign('id_entity')->references('id')->on('entities');
            $table->foreign('id_rol')->references('id')->on('rols');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accesses');
    }
}
