<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $table = 'usuarios';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'apellidos',
        'email', 
        'activo',
        'id_rol',
        'eliminado',
        'nombre', 
        'username',
        'id_empresa'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public $timestamps = true;

    public function rol() {
        return $this->belongsTo('App\Rol', 'id_rol');
    }

    public function empresa() {
        return $this->belongsTo('App\Entity', 'id_empresa');
    }

    public function findForPassport($username) {
        return $this->where('username', $username)->first();
    }

    /**
     * Get all users by eliminado
     */
    public function getAllUsers($eliminado) 
    {
        return DB::select(
            "SELECT 
                U.id,
                U.apellidos,
                U.email, 
                U.activo,
                U.id_rol,
                U.eliminado,
                U.nombre, 
                U.username,
                U.id_empresa,
                R.name,
                R.state
            FROM 
                usuarios U, 
                rols R 
            WHERE 
                U.id_rol = R.id AND 
                U.eliminado = ?
            ORDER BY
                U.created_at DESC"
        , [ $eliminado ]);
    }
}
