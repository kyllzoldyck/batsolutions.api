<?php

use Faker\Generator as Faker;

$factory->define(App\Articulo::class, function (Faker $faker) {
    return [
        'descripcion' => $faker->name,
        'eliminado' => 0,
        'id_proveedor' => rand(1, 30),
        'precio_de_compra' => rand(5000, 100000),
        'precio_de_venta' => rand(5000, 100000),
        'stock' => rand(0, 1000),
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s'),
        'codigo' => str_random(10)
    ];
});
