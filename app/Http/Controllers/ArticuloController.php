<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticuloRequest;
use App\Http\Controllers\Api\AuthController;
use DB;
use App\Proveedor;
use App\Articulo;
use Illuminate\Support\Facades\Log;

class ArticuloController extends Controller
{
    public function index()
    {
        try {

            Log::info('Getting articulos');

            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) 
            {
                Log::warning('Credenciales incorrectas');
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            $articulos = Articulo::where('eliminado', 0)->orderBy('created_at', 'desc')->get();

            if ($user->getData()->id_rol == 1) 
            {
                Log::info('Admin: Getting articulos');
                $articulos->each(function (Articulo $art) {
                    $art->proveedor->empresa;
                });
            } 
            else 
            {   
                Log::info('Getting articulos');
                $proveedores = Proveedor::where('id_empresa', $user->getData()->id_empresa)->pluck('id')->toArray();
                $articulos = Articulo::where('eliminado', 0)->whereIn('id_proveedor', $proveedores)->orderBy('created_at', 'desc')->get();

                for ($i = 0; $i < count($articulos); $i++)
                {
                    if ($articulos[$i]->proveedor->empresa->id == $user->getData()->id_empresa)
                    {
                        $articulos[$i]->proveedor->empresa;
                    }
                }
            }

            Log::info('Articulos obtained');
            return response()->json($articulos, 200);
        } 
        catch (\Exception $e) {    
            return response()->json(['error' => $e], 500);
        }
    }

    public function show($id)
    {
        try {

            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if (! is_numeric($id)) {
                return response()->json(['error' => 'El ID debe ser numérico.'], 404);
            }

            $articulo = Articulo::find($id);
            $articulo->proveedor;

            if ($articulo->eliminado == 1) {
                return response()->json(['error' => 'El artículo esta eliminado.'], 404);
            }

            if ($user->getData()->id_rol == 1) {
                return response()->json($articulo, 200);
            }
            if ($proveedor->id_empresa !== $user->getData()->id_empresa) {
                return response()->json(['error' => 'No tiene permisos para ver este articulo.'], 404);
            }
            
            return response()->json($articulo, 200);

        } catch (\Exception $e) {
            return response()->json($e, 500);
        }
    }

    public function store(ArticuloRequest $request)
    {
        try {

            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if ($user->getData()->id_rol == 1) {
                $proveedor = Proveedor::find($request->input('id_proveedor'))
                    ->where('eliminado', 0)
                    ->first();

            } else {
                $proveedor = Proveedor::find($request->input('id_proveedor'))
                    ->where('id_empresa', $user->getData()->id_empresa)
                    ->where('eliminado', 0)
                    ->first();
            }

            
            if (count($proveedor) == 0) {
                return response()->json(['error' => 'El proveedor no existe o no esta disponible.'], 404);
            }

            $articulo = new Articulo();
            $articulo->descripcion = mb_strtolower(trim($request->input('descripcion')), 'UTF-8');
            $articulo->eliminado = 0;
            $articulo->id_proveedor = trim($request->input('id_proveedor'));
            $articulo->precio_de_compra = trim($request->input('precio_de_compra'));
            $articulo->precio_de_venta = trim($request->input('precio_de_venta'));
            $articulo->stock = trim($request->input('stock'));
            $articulo->codigo = mb_strtoupper((trim($request->input('codigo'))), 'UTF-8');
            $articulo->created_at = Date('Y-m-d H:i:s');
            $articulo->updated_at = Date('Y-m-d H:i:s');

            $articulo->save();

            return response()->json($articulo, 200);
        } catch (\Exception $e) {
            return response()->json($e, 500);
        }
    }

    public function update(ArticuloRequest $request, $id)
    {
        try {

            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            $proveedor = Proveedor::find($request->input('id_proveedor'))
                ->where('id_empresa', $user->getData()->id_empresa)
                ->where('eliminado', 0)
                ->get();

            if (count($proveedor) == 0) {
                return response()->json(['error' => 'El proveedor no existe o no esta disponible.'], 404);
            }

            $articulo = Articulo::find($id)
                ->where('eliminado', 0)
                ->get();

            if (count($articulo) == 0) {
                return response()->json(['error' => 'El articulo no existe o no esta disponible.'], 404);
            }

            $articulo = Articulo::find($id);
            $articulo->descripcion = mb_strtolower(trim($request->input('descripcion')), 'UTF-8');
            $articulo->eliminado = 0;
            $articulo->id_proveedor = trim($request->input('id_proveedor'));
            $articulo->precio_de_compra = trim($request->input('precio_de_compra'));
            $articulo->precio_de_venta = trim($request->input('precio_de_venta'));
            $articulo->stock = trim($request->input('stock'));
            $articulo->codigo = mb_strtoupper((trim($request->input('codigo'))), 'UTF-8');
            $articulo->created_at = Date('Y-m-d H:i:s');
            $articulo->updated_at = Date('Y-m-d H:i:s');
            $articulo->save();

            return response()->json($articulo, 200);

        } catch (\Exception $e) {
            return response()->json($e, 500);
        }
    }

    public function destroy($id)
    {
        try {
            
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if (!is_numeric($id))
                return response()->json(['error' => 'El ID no es numérico.', 200]);

            $articulo = Articulo::find($id);
            $articulo->eliminado = 1;
            $articulo->save();
            
            return response()->json($articulo, 200);

        } catch (\Exception $e) {
            return response()->json($e, 500);
        }
    }
}
