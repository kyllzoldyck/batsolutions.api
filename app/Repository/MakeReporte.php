<?php

namespace App\Repository;

use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Support\Facades\Log;

class MakeReporte extends Fpdf
{
    
    public $font = 'Arial';
    public $fontSize = 7;
    public $fontTitle = 18;
    public $fontSizeBoldHeader = 10;

    public function makeReporteClientesOrProveedores($elements, $user, $title, $empresa)
    {
        $tableHeight = 7;

        $tableWidthPhone = 17;
        $tableWidthRut = 15;
        $tableWidthContacto = 25;
        $tableWidthRazonSocial = 50;
        $tableWidthEmail = 50;
        $tableWidthDireccion = 43;

        $this->AddPage();
        $this->SetMargins(5, 15, 5);
        $this->SetFont($this->font, 'B', 18);

        $this->Image(env('PATH_IMG_LOGO', '../../batsolutions.front/src/assets/images/logos/') . $empresa->url_img, null, null, 30, 30, '');

        $this->Cell(190, 30, $title, 0, 1, 'C');
        
        $this->SetFont($this->font, 'B', 12);
        $this->Cell(25, 7, 'Fecha: ' . date('Y-m-d'), 0, 1, '');
        $this->Cell(25, 7, 'Usuario: ' . $user->username, 0, 1, '');
        $this->Cell(25, 7, 'Empresa: ' . mb_strtoupper($empresa->razon_social, 'UTF-8'), 0, 1, '');
        $this->ln($tableHeight);

        $this->SetFont($this->font, '', $this->fontSize);

        $this->Cell($tableWidthRut,         $tableHeight, 'RUT',           1, 0, 'C');
        $this->Cell($tableWidthRazonSocial, $tableHeight, utf8_decode('RAZÓN SOCIAL'),  1, 0, 'C');
        $this->Cell($tableWidthContacto,    $tableHeight, 'CONTACTO',      1, 0, 'C');
        $this->Cell($tableWidthDireccion,   $tableHeight, utf8_decode('DIRECCIÓN'),     1, 0, 'C');
        $this->Cell($tableWidthPhone,       $tableHeight, utf8_decode('TELÉFONO'),      1, 0, 'C');
        $this->Cell($tableWidthEmail,       $tableHeight, 'EMAIL',         1, 0, 'C');

        $this->ln($tableHeight);

        $cont = 0;
        $countElements = 20;

        foreach($elements as $element) 
        {
            $cont++;

            $this->CustomCell($tableWidthRut,         $tableHeight, $tableWidthRut,            $this->GetX(), $element['rut']);
            $this->CustomCell($tableWidthRazonSocial, $tableHeight, $tableWidthRazonSocial,    $this->GetX(), utf8_decode($element['razon_social']));
            $this->CustomCell($tableWidthContacto,    $tableHeight, $tableWidthContacto,       $this->GetX(), utf8_decode($element['nombre_contacto']));
            $this->CustomCell($tableWidthDireccion,   $tableHeight, $tableWidthDireccion - 10, $this->GetX(), utf8_decode($element['direccion']));
            $this->CustomCell($tableWidthPhone,       $tableHeight, $tableWidthPhone,          $this->GetX(), $element['telefono']);
            $this->CustomCell($tableWidthEmail,       $tableHeight, $tableWidthEmail,          $this->GetX(), $element['email']);
        
            $this->ln($tableHeight);

            if ($cont == $countElements) 
            {
                $this->AddPage();
                $this->SetMargins(5, 15, 5);
                $this->SetFont($this->font, '', $this->fontSize);

                $this->Cell($tableWidthRut,         $tableHeight, 'RUT',                        1, 0, 'C');
                $this->Cell($tableWidthRazonSocial, $tableHeight, utf8_decode('RAZÓN SOCIAL'),  1, 0, 'C');
                $this->Cell($tableWidthContacto,    $tableHeight, 'CONTACTO',                   1, 0, 'C');
                $this->Cell($tableWidthDireccion,   $tableHeight, utf8_decode('DIRECCIÓN'),     1, 0, 'C');
                $this->Cell($tableWidthPhone,       $tableHeight, utf8_decode('TELÉFONO'),      1, 0, 'C');
                $this->Cell($tableWidthEmail,       $tableHeight, 'EMAIL',                      1, 0, 'C');
        
                $this->ln($tableHeight);

                $countElements = 32;
                $cont = 0;
            }
        }

        $uniqueName = uniqid();
        $this->Output('F', env('PATH_PDF', 'documents/') . $uniqueName . '.pdf', true);

        return $uniqueName;
    }

    public function makeReporteFacturas($facturas, $user, $title, $type, $fechaDesde, $fechaHasta, $empresa)
    {
        $tableHeight = 7;

        $tableWidthNumero = 15;
        $tableWidthFecha = 20;
        $tableWidthNombre = 100;
        $tableWidthRut = 15;
        $tableWidthTotal = 30;
        $tableWidthAnulada = 20;

        $this->AddPage();
        $this->SetMargins(5, 15, 5);
        $this->SetFont($this->font, 'B', 18);

        $this->Image(env('PATH_IMG_LOGO', '../../batsolutions.front/src/assets/images/logos/') . $empresa->url_img, null, null, 30, 30, '');

        $this->Cell(190, 30, $title, 0, 1, 'C');
        
        $this->SetFont($this->font, 'B', 12);

        $fechaDesde = \DateTime::createFromFormat('Y-m-d', $fechaDesde);
        $fechaHasta = \DateTime::createFromFormat('Y-m-d', $fechaHasta);

        $this->Cell(25, 7, 'Fecha: ' . date('d-m-Y'),                                                              0, 1, '');
        $this->Cell(25, 7, 'Generado por: ' . $user->username,                                                     0, 1, '');
        $this->Cell(25, 7, ($type == 1 ? 'Cliente: ' : 'Proveedor: ') . utf8_decode($facturas[0]['razon_social']), 0, 1, '');
        $this->Cell(25, 7, 'RUT: ' . $facturas[0]['rut'],                                                          0, 1, '');
        $this->Cell(25, 7, 'Fecha desde: ' . $fechaDesde->format('d-m-Y'),                                                          0, 1, '');
        $this->Cell(25, 7, 'Fecha hasta: ' . $fechaHasta->format('d-m-Y'),                                                          0, 1, '');
        
        $this->ln($tableHeight);

        $this->SetFont($this->font, '', $this->fontSize);

        $this->Cell($tableWidthNumero,  $tableHeight, utf8_decode('N°'),                      1, 0, 'C');
        $this->Cell($tableWidthFecha,   $tableHeight, 'FECHA',                                1, 0, 'C');
        $this->Cell($tableWidthNombre,  $tableHeight, ($type == 1 ? 'CLIENTE' : 'PROVEEDOR'), 1, 0, 'C');
        $this->Cell($tableWidthRut,     $tableHeight, 'RUT',                                  1, 0, 'C');
        $this->Cell($tableWidthTotal,   $tableHeight, 'TOTAL',                                1, 0, 'C');
        $this->Cell($tableWidthAnulada, $tableHeight, utf8_decode('¿ANULADA?'),               1, 0, 'C');

        $this->ln($tableHeight);

        $cont = 0;
        $countElements = 15;

        foreach($facturas as $factura) 
        {
            $cont++;
            $fecha_de_creacion = \DateTime::createFromFormat('Y-m-d H:m:i', $factura['fecha_de_creacion']);

            $this->CustomCell($tableWidthNumero,  $tableHeight, $tableWidthNumero,   $this->GetX(), $factura['numero']);
            $this->CustomCell($tableWidthFecha,   $tableHeight, $tableWidthFecha,    $this->GetX(), $fecha_de_creacion->format('d-m-Y'));
            $this->CustomCell($tableWidthNombre,  $tableHeight, $tableWidthNombre,   $this->GetX(), utf8_decode($factura['razon_social']));
            $this->CustomCell($tableWidthRut,     $tableHeight, $tableWidthRut,      $this->GetX(), $factura['rut']);
            $this->CustomCell($tableWidthTotal,   $tableHeight, $tableWidthTotal,    $this->GetX(), '$' . number_format($factura['total'], 0, '', '.'));
            $this->CustomCell($tableWidthAnulada, $tableHeight, $tableWidthAnulada,  $this->GetX(), ($factura['nula'] == 1 ? 'SI' : 'NO'));
        
            $this->ln($tableHeight);

            if ($cont == $countElements) 
            {
                $this->AddPage();
                $this->SetMargins(5, 15, 5);
                $this->SetFont($this->font, '', $this->fontSize);

                $this->Cell($tableWidthNumero,  $tableHeight, 'N°',                                       1, 0, 'C');
                $this->Cell($tableWidthFecha,   $tableHeight, 'FECHA',                                    1, 0, 'C');
                $this->Cell($tableWidthNombre,  $tableHeight, ($type == 1 ? 'CLIENTE' : 'PROVEEDOR'),     1, 0, 'C');
                $this->Cell($tableWidthRut,     $tableHeight, 'RUT',                                      1, 0, 'C');
                $this->Cell($tableWidthTotal,   $tableHeight, 'TOTAL',                                    1, 0, 'C');
                $this->Cell($tableWidthAnulada, $tableHeight, '¿ANULADA?',                                1, 0, 'C');
        
                $this->ln($tableHeight);

                $countElements = 32;
                $cont = 0;
            }
        }

        $uniqueName = uniqid();
        $this->Output('F', 'documents/' . $uniqueName . '.pdf', false);

        return $uniqueName;
    }

    public function makeReporteDetalleFactura($factura, $title, $user, $valorIva)
    {
        $tableHeight = 7;

        $tableWidthDescripcion = 125;
        $tableWidthPrecioCompra = 25;
        $tableWidthUnidades = 25;
        $tableWidthSubTotal = 25;

        $spaceWidthTotales = 150;
        $tableWidthTotales = 25;
        $tableWidthTotalesValue = 25; 

        $this->AddPage();
        $this->SetMargins(5, 15, 5);
        $this->SetFont($this->font, 'B', 18);

        $this->Image(env('PATH_IMG_LOGO', '../../batsolutions.front/src/assets/images/logos/') . $factura->empresa->url_img, null, null, 30, 30, '');

        $this->Cell(190, 30, $title, 0, 1, 'C');
        
        $this->SetFont($this->font, 'B', 12);

        $this->Cell(25, 7, 'Fecha: ' . date('d-m-Y'), 0, 1, '');

        $this->Cell(25,  $tableHeight, $factura->tipo_factura == 2 ? 'Proveedor: ' : 'Cliente: ',                                0, 0, '');
        $this->Cell(170, $tableHeight, utf8_decode($factura->rut . ' ' . $factura->razon_social),                                0, 1, '');
        $this->Cell(25,  $tableHeight, utf8_decode('Número: '),                                                                  0, 0, '');
        $this->Cell(40,  $tableHeight, $factura->numero,                                                                         0, 0, '');
        $this->Cell(25,  $tableHeight, 'Fecha: ',                                                                                0, 0, '');
        $this->Cell(40,  $tableHeight, \DateTime::createFromFormat('Y-m-d H:i:s', $factura->fecha_de_creacion)->format('d-m-Y'), 0, 0, '');
        $this->Cell(25,  $tableHeight, utf8_decode('Teléfono: '),                                                                0, 0, '');
        $this->Cell(40,  $tableHeight, $factura->telefono,                                                                       0, 1, '');

        $this->ln($tableHeight);

        $this->SetFont($this->font, '', $this->fontSize);

        $this->Cell($tableWidthDescripcion,  $tableHeight, utf8_decode('DESCRIPCIÓN'), 1, 0, 'C');
        $this->Cell($tableWidthPrecioCompra, $tableHeight, 'PRECIO COMPRA',            1, 0, 'C');
        $this->Cell($tableWidthUnidades,     $tableHeight, 'UNIDADES',                 1, 0, 'C');
        $this->Cell($tableWidthSubTotal,     $tableHeight, 'SUB TOTAL',                1, 0, 'C');
        
        $this->ln($tableHeight);

        $sub_total = 0;
        $iva = 0;
        $total = 0;

        foreach($factura->detallesFactura as $detalle)
        {
            $sub_total += $detalle->unidades * $detalle->precio;

            $this->CustomCell($tableWidthDescripcion,  $tableHeight, $tableWidthDescripcion,  $this->GetX(), utf8_decode($detalle->descripcion_articulo),                              'L');
            $this->CustomCell($tableWidthPrecioCompra, $tableHeight, $tableWidthPrecioCompra, $this->GetX(), '$' . number_format($detalle->precio, 0, '', '.'),                        'R');
            $this->CustomCell($tableWidthUnidades,     $tableHeight, $tableWidthUnidades,     $this->GetX(), number_format($detalle->unidades, 0, '', '.'),                            'R');
            $this->CustomCell($tableWidthSubTotal,     $tableHeight, $tableWidthSubTotal,     $this->GetX(), '$' . number_format(($detalle->precio * $detalle->unidades), 0, '', '.'), 'R');

            $this->ln($tableHeight);
        }

        $iva = $factura->exenta == 1 ? 0 : $sub_total * $valorIva;
        $total = $sub_total + $iva;

        $this->ln($tableHeight);

        $this->Cell($spaceWidthTotales,      $tableHeight, '',                                           0, 0, '');
        $this->Cell($tableWidthTotales,      $tableHeight, 'SUB TOTAL',                                  1, 0, 'R');
        $this->Cell($tableWidthTotalesValue, $tableHeight, '$' . number_format($sub_total, 0, '', '.'),  1, 1, 'R');
        
        $this->Cell($spaceWidthTotales,      $tableHeight, '',                                           0, 0, '');
        $this->Cell($tableWidthTotales,      $tableHeight, 'IVA',                                        1, 0, 'R');
        $this->Cell($tableWidthTotalesValue, $tableHeight, '$' . number_format($iva, 0, '', '.'),        1, 1, 'R');

        $this->Cell($spaceWidthTotales,      $tableHeight, '',                                           0, 0, '');
        $this->Cell($tableWidthTotales,      $tableHeight, 'TOTAL',                                      1, 0, 'R');
        $this->Cell($tableWidthTotalesValue, $tableHeight, '$' . number_format($total, 0, '', '.'),      1, 1, 'R');

        $uniqueName = uniqid();
        $this->Output('F', 'documents/' . $uniqueName . '.pdf', false);

        return $uniqueName;
    }

    public function makeReporteArticulos($articulos, $user, $title, $empresa)
    {
        $tableHeight = 7;

        $tableWidthCodigo = 20;
        $tableWidthDescripcion = 60;
        $tableWidthProveedor = 50;
        $tableWidthPrecios = 25;
        $tableWidthStock = 20;

        $this->AddPage();
        $this->SetMargins(5, 15, 5);
        $this->SetFont($this->font, 'B', 18);

        $this->Image(env('PATH_IMG_LOGO', '../../batsolutions.front/src/assets/images/logos/') . $empresa->url_img, null, null, 30, 30, '');

        $this->Cell(190, 30, utf8_decode($title), 0, 1, 'C');
        
        $this->SetFont($this->font, 'B', 12);
        $this->Cell(25, 7, 'Fecha: ' . date('Y-m-d'), 0, 1, '');
        $this->Cell(25, 7, 'Usuario: ' . $user->username, 0, 1, '');

        $this->ln($tableHeight);

        $this->SetFont($this->font, '', $this->fontSize);

        $this->Cell($tableWidthCodigo,             $tableHeight, utf8_decode('CÓDIGO'),      1, 0, 'C');
        $this->Cell($tableWidthDescripcion,        $tableHeight, utf8_decode('DESCRIPCIÓN'), 1, 0, 'C');
        $this->Cell($tableWidthProveedor,          $tableHeight, 'PROVEEDOR',                1, 0, 'C');
        $this->Cell($tableWidthPrecios,            $tableHeight, 'PRECIO COMPRA',            1, 0, 'C');
        $this->Cell($tableWidthPrecios,            $tableHeight, 'PRECIO VENTA',             1, 0, 'C');
        $this->Cell($tableWidthStock,              $tableHeight, 'STOCK',                    1, 0, 'C');

        $this->ln($tableHeight);

        $cont = 0;
        $countElements = 20;

        foreach($articulos as $articulo) 
        {
            $cont++;

            $this->CustomCell($tableWidthCodigo,          $tableHeight, $tableWidthCodigo,          $this->GetX(), utf8_decode($articulo['codigo']));
            $this->CustomCell($tableWidthDescripcion,     $tableHeight, $tableWidthDescripcion,     $this->GetX(), utf8_decode($articulo['descripcion']));
            $this->CustomCell($tableWidthProveedor,       $tableHeight, $tableWidthProveedor,       $this->GetX(), utf8_decode($articulo['proveedor']['razon_social']));
            $this->CustomCell($tableWidthPrecios,         $tableHeight, $tableWidthPrecios,         $this->GetX(), '$' . number_format($articulo['precio_de_compra'], 0, '', '.'), 'R');
            $this->CustomCell($tableWidthPrecios,         $tableHeight, $tableWidthPrecios,         $this->GetX(), '$' . number_format($articulo['precio_de_venta'], 0, '', '.'),  'R');
            $this->CustomCell($tableWidthStock,           $tableHeight, $tableWidthStock,           $this->GetX(), number_format($articulo['stock'], 0, '', '.'),                  'R');
        
            $this->ln($tableHeight);

            if ($cont == $countElements) 
            {
                $this->AddPage();
                $this->SetMargins(5, 15, 5);
                $this->SetFont($this->font, '', $this->fontSize);

                $this->Cell($tableWidthCodigo,             $tableHeight, utf8_decode('CÓDIGO'),      1, 0, 'C');
                $this->Cell($tableWidthDescripcion,        $tableHeight, utf8_decode('DESCRIPCIÓN'), 1, 0, 'C');
                $this->Cell($tableWidthProveedor,          $tableHeight, 'PROVEEDOR',                1, 0, 'C');
                $this->Cell($tableWidthPrecios,            $tableHeight, 'PRECIO COMPRA',            1, 0, 'C');
                $this->Cell($tableWidthPrecios,            $tableHeight, 'PRECIO VENTA',             1, 0, 'C');
                $this->Cell($tableWidthStock,              $tableHeight, 'STOCK',                    1, 0, 'C');
        
                $this->ln($tableHeight);

                $countElements = 32;
                $cont = 0;
            }
        }

        $uniqueName = uniqid();
        $this->Output('F', 'documents/' . $uniqueName . '.pdf', true);

        return $uniqueName;
    }

    public function makeReporteDetallePresupuesto($presupuesto, $title, $user, $valorIva)
    {
        $tableHeight = 7;

        $tableWidthDescripcion = 125;
        $tableWidthPrecioCompra = 25;
        $tableWidthUnidades = 25;
        $tableWidthSubTotal = 25;

        $spaceWidthTotales = 150;
        $tableWidthTotales = 25;
        $tableWidthTotalesValue = 25; 

        $this->AddPage();
        $this->SetMargins(5, 15, 5);
        $this->SetFont($this->font, 'B', 18);

        $this->Image(env('PATH_IMG_LOGO', '../../batsolutions.front/src/assets/images/logos/') . $presupuesto->empresa->url_img, null, null, 30, 30, '');

        $this->Cell(190, 30, $title, 0, 1, 'C');
        
        $this->SetFont($this->font, 'B', $this->fontSizeBoldHeader);
        $this->Cell(40, 7, 'Fecha Documento: ', 0, 0, '');
        $this->SetFont($this->font, '', 10);
        $this->Cell(25, 7, date('d-m-Y'), 0, 1, '');

        #region EMPRESA
        $this->SetFont($this->font, 'B', $this->fontSizeBoldHeader);
        $this->Cell(25,  $tableHeight, 'Empresa: ',                                                                           0, 0, '');
        $this->SetFont($this->font, '', 10);
        $this->Cell(170, $tableHeight, utf8_decode($presupuesto->empresa->razon_social),                                      0, 1, '');

        $this->SetFont($this->font, 'B', $this->fontSizeBoldHeader);
        $this->Cell(25,  $tableHeight, 'Giro: ',                                                                              0, 0, '');
        $this->SetFont($this->font, '', 10);
        $this->Cell(170, $tableHeight, utf8_decode($presupuesto->empresa->giro),                                              0, 1, '');

        $this->SetFont($this->font, 'B', $this->fontSizeBoldHeader);
        $this->Cell(25,  $tableHeight, 'Rut: ',                                                                               0, 0, '');
        $this->SetFont($this->font, '', 10);
        $this->Cell(170, $tableHeight, utf8_decode($presupuesto->empresa->rut),                                               0, 1, '');

        $this->SetFont($this->font, 'B', $this->fontSizeBoldHeader);
        $this->Cell(25,  $tableHeight, 'Contacto: ',                                                                          0, 0, '');
        $this->SetFont($this->font, '', 10);
        $this->Cell(170, $tableHeight, utf8_decode($presupuesto->empresa->name),                                              0, 1, '');
        #endregion EMPRESA

        $this->ln($tableHeight);

        #region CLIENTE
        $this->SetFont($this->font, 'B', $this->fontSizeBoldHeader);
        $this->Cell(25,  $tableHeight, 'Cliente: ',                                                                           0, 0, '');
        $this->SetFont($this->font, '', 10);
        $this->Cell(170, $tableHeight, utf8_decode($presupuesto->cliente_rut . ' ' . $presupuesto->cliente_razon_social),     0, 1, '');

        $this->SetFont($this->font, 'B', $this->fontSizeBoldHeader);
        $this->Cell(25,  $tableHeight, utf8_decode('Número: '),                                                               0, 0, '');
        $this->SetFont($this->font, '', 10);
        $this->Cell(40,  $tableHeight, $presupuesto->id,                                                                      0, 0, '');

        $this->SetFont($this->font, 'B', $this->fontSizeBoldHeader);
        $this->Cell(25,  $tableHeight, utf8_decode('Creación: '),                                                             0, 0, '');
        $this->SetFont($this->font, '', 10);
        $this->Cell(40,  $tableHeight, \DateTime::createFromFormat('Y-m-d H:i:s', $presupuesto->created_at)->format('d-m-Y'), 0, 0, '');

        $this->SetFont($this->font, 'B', $this->fontSizeBoldHeader);
        $this->Cell(25,  $tableHeight, utf8_decode('Teléfono: '),                                                             0, 0, '');
        $this->SetFont($this->font, '', 10);
        $this->Cell(40,  $tableHeight, $presupuesto->cliente_telefono,                                                        0, 1, '');

        $this->SetFont($this->font, 'B', $this->fontSizeBoldHeader);
        $this->Cell(25,  $tableHeight, 'Email: ',                                                                             0, 0, '');
        $this->SetFont($this->font, '', 10);
        $this->Cell(105,  $tableHeight, $presupuesto->cliente_email,                                                          0, 0, '');

        $this->SetFont($this->font, 'B', $this->fontSizeBoldHeader);
        $this->Cell(40,  $tableHeight, utf8_decode('Fecha expiración: '),                                                     0, 0, '');
        $this->SetFont($this->font, '', 10);
        $this->Cell(25,  $tableHeight, \DateTime::createFromFormat('Y-m-d H:i:s', $presupuesto->fecha_expiracion)->format('d-m-Y'), 0, 0, '');
        #endregion CLIENTE
        
        $this->ln(20);

        $this->SetFont($this->font, '', $this->fontSize);

        $this->Cell($tableWidthDescripcion,  $tableHeight, utf8_decode('DESCRIPCIÓN'), 1, 0, 'C');
        $this->Cell($tableWidthPrecioCompra, $tableHeight, 'PRECIO COMPRA',            1, 0, 'C');
        $this->Cell($tableWidthUnidades,     $tableHeight, 'UNIDADES',                 1, 0, 'C');
        $this->Cell($tableWidthSubTotal,     $tableHeight, 'SUB TOTAL',                1, 0, 'C');
        
        $this->ln($tableHeight);

        $sub_total = 0;
        $iva = 0;
        $total = 0;

        foreach($presupuesto->detallesPresupuesto as $detalle)
        {
            $sub_total += $detalle->unidades * $detalle->precio;

            $this->CustomCell($tableWidthDescripcion,  $tableHeight, $tableWidthDescripcion,  $this->GetX(), utf8_decode($detalle->descripcion_articulo),                              'L');
            $this->CustomCell($tableWidthPrecioCompra, $tableHeight, $tableWidthPrecioCompra, $this->GetX(), '$' . number_format($detalle->precio, 0, '', '.'),                        'R');
            $this->CustomCell($tableWidthUnidades,     $tableHeight, $tableWidthUnidades,     $this->GetX(), number_format($detalle->unidades, 0, '', '.'),                            'R');
            $this->CustomCell($tableWidthSubTotal,     $tableHeight, $tableWidthSubTotal,     $this->GetX(), '$' . number_format(($detalle->precio * $detalle->unidades), 0, '', '.'), 'R');

            $this->ln($tableHeight);
        }

        $iva = $presupuesto->exenta == 1 ? 0 : round($sub_total * $valorIva);
        $total = $sub_total + $iva;

        $this->ln($tableHeight);

        $this->Cell($spaceWidthTotales,      $tableHeight, '',                                           0, 0, '');
        $this->Cell($tableWidthTotales,      $tableHeight, 'SUB TOTAL',                                  1, 0, 'R');
        $this->Cell($tableWidthTotalesValue, $tableHeight, '$' . number_format($sub_total, 0, '', '.'),  1, 1, 'R');
        
        $this->Cell($spaceWidthTotales,      $tableHeight, '',                                           0, 0, '');
        $this->Cell($tableWidthTotales,      $tableHeight, 'IVA',                                        1, 0, 'R');
        $this->Cell($tableWidthTotalesValue, $tableHeight, '$' . number_format($iva, 0, '', '.'),        1, 1, 'R');

        $this->Cell($spaceWidthTotales,      $tableHeight, '',                                           0, 0, '');
        $this->Cell($tableWidthTotales,      $tableHeight, 'TOTAL',                                      1, 0, 'R');
        $this->Cell($tableWidthTotalesValue, $tableHeight, '$' . number_format($total, 0, '', '.'),      1, 1, 'R');

        $uniqueName = uniqid();
        $this->Output('F', 'documents/' . $uniqueName . '.pdf', false);

        return $uniqueName;
    }

    public function Footer()
    {
        $this->SetY(-15);
        $this->SetFont('Arial', 'I', 8);
        $this->Cell(0, 10, utf8_decode('Página '). $this->PageNo(), 0, 0, 'C');
    }

    public function CustomCell($w, $h, $l, $x, $t, $align = 'L') 
    {
        $height = $h / 3;
        $first = $height + 2;
        $second = $height + $height + $height + 3;
        $len = strlen($t);

        if ($len > $l)
        {
            $txt = str_split($t, $l - 10);
            $this->SetX($x);
            $this->Cell($w, $first, $txt[0], '', '', '');
            $this->SetX($x);
            $this->Cell($w, $second, $txt[1], '', '', '');
            $this->SetX($x);
            $this->Cell($w, $h, '', 'LTRB', 0, 'L', 0);
        }
        else 
        {
            $this->SetX($x);
            $this->Cell($w, $h, $t, 'LTRB', 0, $align, 0);
        }
    }
}
