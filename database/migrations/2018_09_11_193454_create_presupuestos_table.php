<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresupuestosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presupuestos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cliente_direccion', 200);
            $table->string('cliente_email', 100);
            $table->string('cliente_nombre_contacto', 200)->nullable();
            $table->string('cliente_razon_social');
            $table->string('cliente_rut', 10);
            $table->string('cliente_telefono', 15);
            $table->string('cliente_giro', 200);
            $table->dateTime('fecha_expiracion');
            $table->string('nombre', 100);
            $table->string('email', 100);
            $table->string('asunto', 150);
            $table->text('descripcion');
            $table->integer('id_empresa')->unsigned();
            $table->boolean('eliminado');
            $table->timestamps();
            $table->integer('numero');
            $table->boolean('exenta');

            $table->foreign('id_empresa')->references('id')->on('entities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presupuestos');
    }
}
