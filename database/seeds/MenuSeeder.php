<?php

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # 1
        DB::table('menus')->insert([
            'name' => 'facturación',
            'icon' => '<i class="fa fa-usd" aria-hidden="true"></i>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1,
            'uri' => 'facturacion'
        ]);

        # 2
        DB::table('menus')->insert([
            'name' => 'factura compra',
            'icon' => '',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1,
            'uri' => 'compra'
        ]);

        # 3
        DB::table('menus')->insert([
            'name' => 'factura venta',
            'icon' => '',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1,
            'uri' => 'venta'
        ]);
        
        # 4
        DB::table('menus')->insert([
            'name' => 'nota de credito',
            'icon' => '',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1,
            'uri' => 'nota-credito'
        ]);

        # 5
        DB::table('menus')->insert([
            'name' => 'artículos',
            'icon' => '<i class="fa fa-cube" aria-hidden="true"></i>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1,
            'uri' => 'articulos'
        ]);
        
        # 6
        DB::table('menus')->insert([
            'name' => 'clientes',
            'icon' => '<i class="fa fa-handshake-o" aria-hidden="true"></i>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1,
            'uri' => 'clientes'
        ]);
        
        # 7
        DB::table('menus')->insert([
            'name' => 'proveedores',
            'icon' => '<i class="fa fa-truck" aria-hidden="true"></i>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1,
            'uri' => 'proveedores'
        ]);
        
        # 8
        DB::table('menus')->insert([
            'name' => 'usuarios',
            'icon' => '<i class="fa fa-users" aria-hidden="true"></i>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1,
            'uri' => 'usuarios'
        ]);
        
        # 9
        DB::table('menus')->insert([
            'name' => 'reportes',
            'icon' => '<i class="fas fa-file" aria-hidden="true"></i>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1,
            'uri' => 'reportes'
        ]);

        # 10
        DB::table('menus')->insert([
            'name' => 'clientes',
            'icon' => '',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1,
            'uri' => 'clientes'
        ]);

        # 11
        DB::table('menus')->insert([
            'name' => 'proveedores',
            'icon' => '',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1,
            'uri' => 'proveedores'
        ]);

        # 12
        DB::table('menus')->insert([
            'name' => 'facturas por cliente',
            'icon' => '',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1,
            'uri' => 'facturas-clientes'
        ]);

        # 13
        DB::table('menus')->insert([
            'name' => 'facturas por proveedor',
            'icon' => '',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1,
            'uri' => 'facturas-proveedores'
        ]);

        # 14
        DB::table('menus')->insert([
            'name' => 'artículos',
            'icon' => '',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1,
            'uri' => 'articulos'
        ]);

        # 15
        DB::table('menus')->insert([
            'name' => 'permisos',
            'icon' => '<i class="fa fa-lock" aria-hidden="true"></i>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1,
            'uri' => 'permisos'
        ]);

        # 15
        DB::table('menus')->insert([
            'name' => 'presupuestos',
            'icon' => '<i class="fa fa-table" aria-hidden="true"></i>',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'state' => 1,
            'uri' => 'presupuestos'
        ]);
    }
}
