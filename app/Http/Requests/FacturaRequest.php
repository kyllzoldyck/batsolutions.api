<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Factura;

class FacturaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'descripcion' => 'required|max:255',
            'fecha' => 'required|date',
            'detalles_factura' => 'required|array',
            'detalles_factura.*.codigo_articulo' => 'required|max:100',
            'detalles_factura.*.descripcion_articulo' => 'required|max:255',
            'detalles_factura.*.iva' => 'required|numeric',
            'detalles_factura.*.unidades' => 'required|numeric',
            'detalles_factura.*.precio' => 'required|numeric',
            'detalles_factura.*.id_articulo' => 'required|numeric',
            'iva' => 'required|numeric',
            'numero' => 'required|numeric',
            'rut' => 'required|max:10',
            'tipo_factura' => 'required|numeric',
            'exenta' => 'required|boolean',
            'giro' => 'required'
        ];
        switch ($this->method()) {
            case 'PUT':
                $rules = [
                    'rut' => 'required|max:10',
                    'descripcion' => 'required|max:255',
                    'fecha' => 'required|date',
                    'giro' => 'required'
                ];
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'descripcion.required' => 'El campo descripción es obligatorio.',
            'descripcion.max:255' => 'La descripción supera el máximo de caracateres.',

            'fecha.required' => 'La fecha es obligatoria.',
            'fecha.date' => 'La fecha no cumple con el formato permitido.',

            'email.required' => 'El campo email es obligatorio.',
            'email.max:100' => 'El email supera el máximo de caracteres.',

            'detalles_factura.required' => 'No existe un detalle para la factura.',
            'detalles_factura.array' => 'El detalle factura no tiene el formato correcto.',

            'detalles_factura.*.codigo_articulo' => [
                'required' => 'El código del articulo es obligatorio.',
                'max:100' => 'El código del articulo supera el máximo de caracteres'
            ],
            'detalles_factura.*.descripcion_articulo' => [
                'required' => 'La descripción del articulo es obligatoria.',
                'max:255' => 'La descripción supera el máximo de caracteres.'
            ],
            'detalles_factura.*.iva' => [
                'required' => 'El iva del articulo es obligatorio.',
                'numeric' => 'El valor del ivá es incorrecto.'
            ],
            'detalles_factura.*.unidades' => [
                'required' => 'Las unidades del articulo son obligatorias.',
                'numeric' => 'El valor de unidades no es correcto.'
            ],
            'detalles_factura.*.precio' => [
                'required' => 'El precio del articulo es obligatorio.',
                'numeric' => 'El valor del precio no es correcto.'
            ],
            'detalles_factura.*.id_articulo' => [
                'required' => 'El ID del articulo es obligatorio.',
                'numeric' => 'El ID del articulo debe ser numerico'
            ],

            'iva.required' => 'El campo iva es obligatorio.',
            'iva.numeric' => 'El valor del iva no es el correcto.',

            'numero.required' => 'El campo número de factura es obligatorio.',
            'numero.numeric' => 'El valor número de factura no es el correcto.',
            
            'rut.required' => 'No existe un rut de la entidad asociado a la factura.',
            'rut.max:10' => 'El rut supera el máximo de caracteres.',
            
            'telefono.required' => 'El campo telefono es obligatorio.',
            'telefono.numeric' => 'El valor del telefono no es el correcto.',

            'tipo_factura.required' => 'El tipo de factura es requerido.',
            'tipo_factura.numeric' => 'El valor de tipo factura no es el correcto.',

            'exenta.required' => 'El tipo exenta no es válido.',
            'exenta.boolean' => 'El tipo exenta no es válido.',

            'giro.required' => 'El giro de la empresa es requerido.'
        ];
    }
}
