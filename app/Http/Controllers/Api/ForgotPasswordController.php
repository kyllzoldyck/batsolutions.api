<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use App\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use DB;
use App\Mail\PasswordResetMail;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\Repository\ForgotPasswordTemplateEmail;

class ForgotPasswordController extends Controller
{
    public function forgot(Request $request)
    {
        try
        {
            if (empty($request->input('email')))
            {
                return response()->json(['error' => 'Debe ingresar un correo para recuperar su contraseña.'], 400);
            }

            $email = trim($request->input('email'));

            $user = User::where('email', $email)->first();

            if (count($user) == 0)
            {
                return response()->json(['error' => 'El usuario no existe en la base de datos.'], 400);
            }

            $token = Crypt::encryptString($user->email . uniqid());

            $resetExists = DB::table('password_resets')->where('email', $email)->first();

            if (count($resetExists) > 0)
            {
                DB::table('password_resets')
                    ->where('email', $email)
                    ->update(['token' => $token, 'created_at' => date('Y-m-d H:i:s')]);
            }
            else 
            {
                DB::table('password_resets')
                    ->insert(['email' => $email, 'token' => $token, 'created_at' => date('Y-m-d H:i:s')]);
            }

            $mail = new PHPMailer(true);

            try{
    
                $forgotPasswordTemplateEmail = new ForgotPasswordTemplateEmail();
    
                $mail->isSMTP();
                $mail->CharSet = 'UTF-8';
                $mail->SMTPAuth = true;
                $mail->SMTPSecure = 'ssl';
                $mail->Host = APP_MAIL_HOST;
                $mail->Port = 465;
                $mail->Username = APP_MAIL_USERNAME;
                $mail->Password = APP_MAIL_PASSWORD;
                $mail->setFrom(APP_MAIL_FROM, 'Bat Solutions'); 
                $mail->Subject = 'Restablecer Contraseña - Sistema Administración de Documentos Tributarios Electrónicos - BAT Solutions SPA';
                $mail->isHTML(true);
                $mail->MsgHtml($forgotPasswordTemplateEmail->forgotPasswordTemplate($user, $token));
                $mail->addAddress($request->input('email'), $user->nombre . ' ' . $user->apellidos); 
    
                $mail->send();
    
                return response()->json(['status' => true], 200);
            } 
            catch(\Exception $e)
            {
                dd($e);
                Log::error($e);
                return response()->json(['error' => 'Intentelo más tarde.'], 500);
            } 

            return response()->json(['status' => true], 200);
        }
        catch (\Exception $e)
        {
            Log::error($e);
            return response()->json(['error' => 'Intentelo más tarde.'], 500);
        }
    }
}
