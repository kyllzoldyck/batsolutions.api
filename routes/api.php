<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/authenticate', 'Api\AuthController@authenticate')->name('auth.authenticate');
Route::get('/authenticated', 'Api\AuthController@getAuthenticatedUser')->name('auth.authenticated');
// Route::put('/password', 'Api\AuthController@updatePassword')->name('auth.updatePassword');
Route::post('forgot/password', 'Api\ForgotPasswordController@forgot')->name('forgot.password');
Route::post('password/reset', 'Api\ResetPasswordController@reset')->name('password.reset');

Route::apiResource('/articulos', 'ArticuloController');
Route::apiResource('/proveedores', 'ProveedorController');
Route::apiResource('/clientes', 'ClienteController');
Route::apiResource('/usuarios', 'UserController');
// Route::apiResource('/menus', 'MenuController', ['only' => 'index']);
// Route::apiResource('/roles', 'RolController', ['only' => 'index']);
// Route::apiResource('/empresas', 'EmpresaController', ['only' => ['index', 'show']]);

/* Maintenance */

Route::group(['prefix' => 'menus'], function () {
    Route::get('/', 'MenuController@index')->name('menus.index');
    Route::get('/{id}', 'MenuController@show')->name('menus.show');
    Route::post('/', 'MenuController@store')->name('menus.store');
    Route::put('/{id}', 'MenuController@update')->name('menus.update');    
    Route::delete('/{id}', 'MenuController@destroy')->name('menus.destroy');
});

Route::group(['prefix' => 'roles'], function () {
    Route::get('/', 'RolController@index')->name('roles.index');
    Route::get('/{id}', 'RolController@show')->name('roles.show');
    Route::post('/', 'RolController@store')->name('roles.store');
    Route::put('/{id}', 'RolController@update')->name('roles.update');    
    Route::delete('/{id}', 'RolController@destroy')->name('roles.destroy');
});

Route::group(['prefix' => 'entities'], function () {
    Route::get('/', 'EntityController@index')->name('entities.index');
    Route::get('/{id}', 'EntityController@show')->name('entities.show');
    Route::post('/', 'EntityController@store')->name('entities.store');
    Route::put('/{id}', 'EntityController@update')->name('entities.update');    
    Route::delete('/{id}', 'EntityController@destroy')->name('entities.destroy');
});

Route::group(['prefix' => 'print_menus'], function () {
    Route::get('/', 'PrintMenuController@index')->name('print_menus.index');
    Route::get('/{id}', 'PrintMenuController@show')->name('print_menus.show');
    Route::post('/', 'PrintMenuController@store')->name('print_menus.store');
    Route::put('/{id}', 'PrintMenuController@update')->name('print_menus.update');
    Route::delete('/{id}', 'PrintMenuController@destroy')->name('print_menus.destroy');
    Route::get('/parent-menus', 'PrintMenuController@getParentMenus')->name('print_menus.getParentMenus');
});

Route::group(['prefix' => 'accesses'], function () {
    Route::get('/roles/entities', 'AccessController@getAccessesByRolAndEntity')->name('accesses.getAccessesByRolAndEntity');
    Route::get('/', 'AccessController@index')->name('accesses.index');
    Route::post('/', 'AccessController@store')->name('accesses.store');
    Route::put('/{id}', 'AccessController@update')->name('accesses.update');
    Route::delete('/{id}', 'AccessController@destroy')->name('accesses.destroy');
});

/* End Maintenance */

Route::prefix('proveedores')->group(function () {
    Route::get('empresas/{id}', 'ProveedorController@getProveedoresByIdEmpresa')->name('proveedores.getProveedoresByIdEmpresa');
    Route::get('rut/{rut}', 'ProveedorController@getProveedorByRut')->name('proveedores.getProveedorByRut');
});

Route::prefix('clientes')->group(function() {
    Route::get('empresas/{id}', 'ClienteController@getClientesByIdEmpresa')->name('clientes.getClientesByIdEmpresa');
    Route::get('rut/{rut}', 'ClienteController@getClienteByRut')->name('clientes.getClienteByRut');
});

Route::prefix('facturas')->group(function () {
    Route::post('', 'FacturaController@store')->name('facturas.store');
    Route::get('compra', 'FacturaController@getFacturasCompra')->name('facturas.getFacturasCompra');
    Route::get('compra/{numero}', 'FacturaController@getFacturaCompraByNumero')->name('facturas.getFacturaCompraByNumero');
    Route::get('venta', 'FacturaController@getFacturasVenta')->name('facturas.getFacturasVenta');
    Route::get('venta/{numero}', 'FacturaController@getFacturaVentaByNumero')->name('facturas.getFacturaVentaByNumero');
    Route::post('nota-credito', 'FacturaController@addNotaCredito')->name('facturas.addNotaCredito');
    Route::get('{id}', 'FacturaController@show')->name('facturas.show');
    Route::put('{id}', 'FacturaController@update')->name('facturas.update');
    Route::delete('{id}', 'FacturaController@destroy')->name('facturas.destroy');
    Route::get('/', 'FacturaController@index')->name('facturas.index');
    Route::get('/numero/{numero}', 'FacturaController@getFacturasByNumero')->name('facturas.getFacturasByNumero');
});

Route::group(['prefix' => 'impuestos'], function() {
    Route::get('/', 'ImpuestoController@index')->name('impuestos.index');
    Route::get('/{id}', 'ImpuestoController@show')->name('impuestos.show');
    Route::get('/impuesto/{nombre}', 'ImpuestoController@getImpuestoByNombre')->name('impuestos.getImpuestoByNombre');
});

Route::prefix('notas')->group(function () {
    Route::get('/', 'NotaCreditoController@index')->name('notas.index');
    Route::get('/{id}', 'NotaCreditoController@show')->name('notas.show');
    Route::post('/', 'NotaCreditoController@store')->name('notas.store');
    Route::put('/{id}', 'NotaCreditoController@update')->name('notas.update');
    Route::delete('/{id}', 'NotaCreditoController@destroy')->name('notas.destroy');
    Route::get('numero/{numero}', 'NotaCreditoController@getNotaCreditoByNumero')->name('notas.getNotaCreditoByNumero');
});

Route::prefix('reportes')->group(function () {
    Route::get('clientes', 'ReporteController@getReporteClientes')->name('reportes.getReporteClientes');
    Route::get('proveedores', 'ReporteController@getReporteProveedores')->name('reportes.getReporteProveedores');
    Route::get('facturas/clientes/{rut}', 'ReporteController@getReporteFacturasByRutCliente')->name('reportes.facturas.getReporteFacturasByRutCliente');
    Route::get('facturas/proveedores/{rut}', 'ReporteController@getReporteFacturasByRutProveedor')->name('reportes.facturas.proveedores.getReporteFacturasByRutProveedor');
    Route::get('articulos/proveedores/{id}', 'ReporteController@getReporteArticulos')->name('reportes.getReporteArticulos');
    Route::post('generate/clientes', 'ReporteController@makeReporteClientes')->name('reportes.makeReporteClientes');
    Route::post('generate/proveedores', 'ReporteController@makeReporteProveedores')->name('reportes.makeReporteProveedores');
    Route::post('generate/facturas-proveedor', 'ReporteController@makeReporteFacturasProveedor')->name('reportes.makeReporteFacturasProveedor');
    Route::post('generate/facturas-cliente', 'ReporteController@makeReporteFacturasCliente')->name('reportes.makeReporteFacturasCliente');
    Route::post('generate/articulos', 'ReporteController@makeReporteArticulos')->name('reportes.makeReporteArticulos');
    Route::get('generate/detalle-factura/{id}', 'ReporteController@makeReporteDetalleFactura')->name('reportes.makeReporteDetalleFactura');
    Route::get('generate/detalle-presupuesto/{id}', 'ReporteController@makeReporteDetallePresupuesto')->name('reportes.makeReporteDetallePresupuesto');
});

Route::prefix('presupuestos')->group(function () {
    Route::get('/', 'PresupuestoController@index')->name('presupuestos.index');
    Route::post('/', 'PresupuestoController@store')->name('presupuestos.store');
    Route::put('/{id}', 'PresupuestoController@update')->name('presupuestos.update');
    Route::delete('/{id}', 'PresupuestoController@destroy')->name('presupuestos.destroy');
    Route::get('/{id}', 'PresupuestoController@getPresupuesto')->name('presupuestos.getPresupuesto');
    Route::post('/send-email/{id}', 'PresupuestoController@sendPresupuestoEmail')->name('presupuestos.sendPresupuestoEmail');
    Route::get('/default-email/{id}', 'PresupuestoController@getDefaultEmailPresupuesto')->name('presupuestos.getDefaultEmailPresupuesto');
});

