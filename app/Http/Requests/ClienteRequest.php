<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Cliente;

class ClienteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $cliente = Cliente::find($this->cliente);

        $rules = [
            'direccion' => 'required|max:200',
            'email' => 'required|unique:clientes|max:100',
            'nombre_contacto' => 'required|max:200',
            'razon_social' => 'required|max:255',
            'rut' => 'required|unique:clientes|max:10',
            'telefono' => 'required|numeric',
            'giro' => 'required|max:300'
        ];

        switch($this->method()) {
            case 'PUT':
                $rules = [
                    'direccion' => 'required|max:200',
                    'email' => 'required|unique:clientes,email,'.$cliente->id.'|max:100',
                    'nombre_contacto' => 'required|max:200',
                    'razon_social' => 'required|max:255',
                    'telefono' => 'required|numeric',
                    'id_empresa' => 'required|numeric',
                    'giro' => 'required|max:300'
                ];
                break;
            default: break;
        }

        return $rules;
    }

    public function messages() 
    {
        return [
            'direccion.required' => 'El campo dirección es obligatorio.',
            'direccion.max:200' => 'El campo dirección supera el máximo de caracteres.',

            'email.required' => 'El campo email es obligatorio.',
            'email.unique' => 'El email ya existe.',
            'email.max:100' => 'El campo email supera el máximo de caracteres.',

            'nombre_contacto.required' => 'El campo nombre contacto es obligatorio.',
            'nombre_contacto.max:200' => 'El campo nombre contacto supera el máximo de careceteres.',

            'razon_social.required' => 'El campo razon social es obligatorio.',
            'razon_social.max:255' => 'El campo razon social supera el máximo de careceteres.',

            'rut.required' => 'El campo rut es obligatorio.',
            'rut.max:10' => 'El campo rut supera el máximo de caracteres.',
            'rut.unique' => 'El rut ya existe.',

            'telefono.required' => 'El campo telefono es obligatorio.',
            'telefono.numeric' => 'El telefono debe ser numérico.',

            'id_empresa.required' => 'El campo empresa es obligatorio.',
            'id_empresa.numeric' => 'El valor empresa no es correcto.',

            'giro.required' => 'El campo giro es obligatorio.',
            'giro.max:300' => 'El campo giro supera el máximo de caracteres.'
        ];
    }
}
