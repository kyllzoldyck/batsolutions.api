<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RolRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:100|min:5',
            'state' => 'required|numeric|boolean'
        ];

        switch ($this->method())
        {
            case 'DELETE':
                $rules = [
                    'id' => 'required|numeric'
                ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'id.required' => 'El id del rol es obligatorio.',
            'id.numeric' => 'El id del rol debe ser numérico.',

            'name.required' => 'El nombre del rol es obligatorio.',
            'name.max:100' => 'El nombre del rol debe tener un máximo de 100 caracteres.',
            'name.min:5' => 'El nombre del rol debe tener un minimo de 5 caracteres.',

            'state.required' => 'El estado del rol es obligatorio.',
            'state.numeric' => 'El estado es de tipo numérico.',
            'state.boolean' => 'El estado debe ser 1 o 0.'
        ];
    }
}
