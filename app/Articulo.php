<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Articulo extends Model
{
    
    protected $table = 'articulos';

    protected $fillable = [
        'descripcion',
        'eliminado',
        'id_proveedor',
        'precio_de_compra',
        'precio_de_venta',
        'stock',
        'codigo'
    ];

    public $timestamps = true;

    public function proveedor() {
        return $this->belongsTo('App\Proveedor', 'id_proveedor');
    }

    
}
