<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MenuSeeder::class);
        $this->call(RolSeeder::class);
        $this->call(EntitySeeder::class);
        $this->call(PrintMenuSeeder::class);
        $this->call(AccessSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ImpuestosTableSeeder::class);
        $this->call(ProveedoresTableSeeder::class);
        $this->call(ClientesTableSeeder::class);
        $this->call(ArticuloTableSeeder::class);
        $this->call(FacturasTableSeeder::class);
    }
}
