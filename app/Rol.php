<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    protected $table = 'rols';

    protected $fillable = [
        'id',
        'name',
        'state'
    ];

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $connection = '';

    function accesses() 
    {
        return $this->hasMany('App\Access', 'id_rol', 'id');
    }

    /**
     * Get Rol By Id And State
     */
    function getRolWithState($id, $state)
    {
        return Rol::where('state', $state)->where('id', $id)->first();
    }

    /**
     * Get Rol By Id 
     */
    function getRol($id)
    {
        return Rol::find($id);
    }

    /**
     * Get Rols By State
     */
    function getRols($state)
    {
        return Rol::where('state', $state)->get();
    }
}
