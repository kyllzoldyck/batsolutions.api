<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ReporteRequest;
use App\Http\Controllers\Api\AuthController;
use App\Cliente;
use App\Proveedor;
use App\Factura;
use App\Entity;
use App\Articulo;
use App\Impuesto;
use App\Repository\MakeReporte;
use App\Presupuesto;
use DB;
use PDF;
use Illuminate\Support\Facades\Log;

class ReporteController extends Controller
{
    public function getReporteClientes(Request $request) 
    {
        try {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
    
            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }
    
            $clientes = [];

            if ($user->getData()->id_rol == 1) {
                
                if (! is_numeric($request->input('empresa'))) {
                    return response()->json(['error' => 'El ID empresa debe ser numérico.'], 404);
                }

                if ($request->input('empresa') == 0) {
                    $clientes = Cliente::where('eliminado', 0)->orderBy('created_at', 'desc')->get();
                }
                else {
                    $clientes = Cliente::where('eliminado', 0)
                        ->where('id_empresa', $request->input('empresa'))
                        ->orderBy('created_at', 'desc')
                        ->get();
                }
            }
            else {
                $clientes = Cliente::where('eliminado', 0)
                    ->where('id_empresa', $user->getData()->id_empresa)
                    ->orderBy('created_at', 'desc')
                    ->get();
            }

            return response()->json($clientes, 200);

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['error' => 'Intentelo más tarde.'], 500);
        }
    }

    public function makeReporteClientes(Request $request)
    {
        try
        {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
    
            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            $clientes = $request->input('clientes');

            if (!is_array($clientes))
            {
                Log::warning('Clientes no es un array');
                return response()->json(['error' => 'No se han podido obtener los clientes para crear el reporte.', 'generate' => false], 400);
            }

            if (count($clientes) == 0)
            {
                Log::info('El arreglo de clientes es 0');
                return response()->json(['error' => 'La cantidad de clientes debe ser mayor a 0'], 400);
            }
  
            $empresa = $user->getData()->empresa;

            if ($user->getData()->id_rol == 1) { 

                if (!is_numeric($request->input('empresa')))
                {
                    Log::warning('El id de la empresa no es numérico.');
                    return response()->json(['error' => 'No se ha podido obtener el reporte para la empresa.']);
                }          

                if ($request->input('empresa') != 0)
                    $empresa = Entity::find($request->input('empresa'));
            }

            $makeReporte = new MakeReporte();
            $reporte = $makeReporte->makeReporteClientesOrProveedores($clientes, $user->getData(), 'CLIENTES', $empresa);
            $fullPath = 'documents/' . $reporte . '.pdf';

            return response()->file($fullPath, ['Content-Type' => 'application/pdf'])->deleteFileAfterSend(true);
        }
        catch (\Exception $e)
        {
            Log::error($e);
            return response()->json(['generate' => false, 'error' => 'Intentelo más tarde'], 500);
        }
    }

    public function getReporteProveedores(Request $request) 
    {
        try {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            $proveedores = [];

            if ($user->getData()->id_rol == 1) {

                if (!is_numeric($request->input('empresa'))) {
                    return response()->json(['error' => 'El ID empresa debe ser numérico.'], 404);
                }

                if ($request->input('empresa') == 0) {
                    $proveedores = Proveedor::where('eliminado', 0)->orderBy('created_at', 'desc')->get();
                } else {
                    $proveedores = Proveedor::where('eliminado', 0)
                        ->where('id_empresa', $request->input('empresa'))
                        ->orderBy('created_at', 'desc')
                        ->get();
                }
            } else {
                $proveedores = Proveedor::where('eliminado', 0)
                    ->where('id_empresa', $user->getData()->id_empresa)
                    ->orderBy('created_at', 'desc')
                    ->get();
            }

            return response()->json($proveedores, 200);

        } catch (\Exception $e) {
            return response()->json($e, 500);
        }
    }

    public function makeReporteProveedores(Request $request)
    {
        try
        {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
    
            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            $proveedores = $request->input('proveedores');

            if (!is_array($proveedores))
            {
                Log::warning('Proveedores no es un array');
                return response()->json(['error' => 'No se han podido obtener los proveedores para crear el reporte.', 'generate' => false], 400);
            }

            if (count($proveedores) == 0)
            {
                Log::info('El arreglo de proveedores es 0');
                return response()->json(['error' => 'La cantidad de proveedores debe ser mayor a 0'], 400);
            }

            $empresa = $user->getData()->empresa;

            if ($user->getData()->id_rol == 1) { 

                if (!is_numeric($request->input('empresa')))
                {
                    Log::warning('El id de la empresa no es numérico.');
                    return response()->json(['error' => 'No se ha podido obtener el reporte para la empresa.']);
                }          

                if ($request->input('empresa') != 0)
                    $empresa = Entity::find($request->input('empresa'));
            }

            $makeReporte = new MakeReporte();
            $reporte = $makeReporte->makeReporteClientesOrProveedores($proveedores, $user->getData(), 'PROVEEDORES', $empresa);
            $fullPath = 'documents/' . $reporte . '.pdf';

            return response()->file($fullPath)->deleteFileAfterSend(true);
        }
        catch (\Exception $e)
        {
            Log::error($e);
            return response()->json(['generate' => false, 'error' => 'Intentelo más tarde'], 500);
        }
    }

    public function getReporteFacturasByRutCliente(Request $request, $rut)
    {
        try {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
    
            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }
            
            $facturas = [];

            if ($user->getData()->id_rol == 1) {
                if ($request->input('empresa') == 0 || empty($request->input('empresa'))) {
                    $facturas = DB::select(
                        'SELECT f.id, f.descripcion, f.email, f.nula, f.eliminado, f.fecha, f.fecha_de_creacion, f.iva, f.numero, f.tipo_factura, f.razon_social, f.direccion, f.rut, f.telefono, f.nombre_contacto, f.id_empresa, ROUND(SUM((df.precio * df.unidades) + df.iva), 0) AS total, SUM(df.unidades * df.precio) AS subTotal
                        FROM facturas f, detalle_factura df
                        WHERE f.id = df.id_factura AND f.eliminado = 0 AND f.rut = ? AND DATE(f.fecha) >= ? AND DATE(f.fecha) <= ?
                        GROUP BY f.id ORDER BY f.fecha_de_creacion DESC', [$rut, date('Y-m-d', strtotime($request->input('fechaDesde'))), date('Y-m-d', strtotime($request->input('fechaHasta')))]);
                } else {
                    $facturas = DB::select(
                        'SELECT f.id, f.descripcion, f.email, f.nula, f.eliminado, f.fecha, f.fecha_de_creacion, f.iva, f.numero, f.tipo_factura, f.razon_social, f.direccion, f.rut, f.telefono, f.nombre_contacto, f.id_empresa, ROUND(SUM((df.precio * df.unidades) + df.iva), 0) AS total, SUM(df.unidades * df.precio) AS subTotal
                        FROM facturas f, detalle_factura df
                        WHERE f.id = df.id_factura AND f.eliminado = 0 AND f.rut = ? AND DATE(f.fecha) >= ? AND DATE(f.fecha) <= ? AND f.id_empresa = ?
                        GROUP BY f.id ORDER BY f.fecha_de_creacion DESC', [$rut, date('Y-m-d', strtotime($request->input('fechaDesde'))), date('Y-m-d', strtotime($request->input('fechaHasta'))), $request->input('empresa')]
                    );   
                }
            }
            else {
                $facturas = DB::select(
                    'SELECT f.id, f.descripcion, f.email, f.nula, f.eliminado, f.fecha, f.fecha_de_creacion, f.iva, f.numero, f.tipo_factura, f.razon_social, f.direccion, f.rut, f.telefono, f.nombre_contacto, f.id_empresa, ROUND(SUM((df.precio * df.unidades) + df.iva), 0) AS total, SUM(df.unidades * df.precio) AS subTotal
                    FROM facturas f, detalle_factura df
                    WHERE f.id = df.id_factura AND f.eliminado = 0 AND f.rut = ? AND f.id_empresa = ? AND DATE(f.fecha) >= ? AND DATE(f.fecha) <= ?  
                    GROUP BY f.id ORDER BY f.fecha_de_creacion DESC', [$rut, $user->getData()->id_empresa, date('Y-m-d', strtotime($request->input('fechaDesde'))), date('Y-m-d', strtotime($request->input('fechaHasta')))]);
            }

            return response()->json($facturas, 200);
            
        } catch (\Exception $e) {
            return response()->json($e, 500);
        }
    }

    public function makeReporteFacturasCliente(Request $request) 
    {
        try
        {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
    
            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            $facturas = $request->input('facturas');

            if (!is_array($facturas))
            {
                Log::warning('facturas no es un array');
                return response()->json(['error' => 'No se ha podido obtener las facturas para crear el reporte.', 'generate' => false], 400);
            }

            if (count($facturas) == 0)
            {
                Log::info('El arreglo de facturas es 0');
                return response()->json(['error' => 'La cantidad de facturas debe ser mayor a 0'], 400);
            }

            $empresa = $user->getData()->empresa;

            if ($user->getData()->id_rol == 1) { 

                if (!is_numeric($request->input('empresa')))
                {
                    Log::warning('El id de la empresa no es numérico.');
                    return response()->json(['error' => 'No se ha podido obtener el reporte para la empresa.']);
                }          

                if ($request->input('empresa') != 0)
                    $empresa = Entity::find($request->input('empresa'));
            }

            $makeReporte = new MakeReporte();
            $reporte = $makeReporte->makeReporteFacturas($facturas, $user->getData(), 'FACTURAS PROVEEDOR', 2, $request->input('fechaDesde'), $request->input('fechaHasta'), $empresa);
            $fullPath = 'documents/' . $reporte . '.pdf';

            return response()->file($fullPath)->deleteFileAfterSend(true);
        }
        catch (\Exception $e)
        {
            Log::error($e);
            return response()->json(['generate' => false, 'error' => 'Intentelo más tarde'], 500);
        }
    }

    public function getReporteFacturasByRutProveedor(Request $request, $rut) 
    {
        try {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
    
            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            $facturas = [];

            if ($user->getData()->id_rol == 1) {
                if ($request->input('empresa') == 0 || empty($request->input('empresa'))) {
                    $facturas = DB::select(
                        'SELECT f.id, f.descripcion, f.email, f.nula, f.eliminado, f.fecha, f.fecha_de_creacion, f.iva, f.numero, f.tipo_factura, f.razon_social, f.direccion, f.rut, f.telefono, f.nombre_contacto, f.id_empresa, ROUND(SUM((df.precio * df.unidades) + df.iva), 0) AS total, SUM(df.unidades * df.precio) AS subTotal
                        FROM facturas f, detalle_factura df
                        WHERE f.id = df.id_factura AND f.eliminado = 0 AND f.rut = ? AND DATE(f.fecha) >= ? AND DATE(f.fecha) <= ?
                        GROUP BY f.id ORDER BY f.fecha_de_creacion DESC',
                        [$rut, $request->input('fechaDesde'), $request->input('fechaHasta')]);
                } else {
                    $facturas = DB::select(
                        'SELECT f.id, f.descripcion, f.email, f.nula, f.eliminado, f.fecha, f.fecha_de_creacion, f.iva, f.numero, f.tipo_factura, f.razon_social, f.direccion, f.rut, f.telefono, f.nombre_contacto, f.id_empresa, ROUND(SUM((df.precio * df.unidades) + df.iva), 0) AS total, SUM(df.unidades * df.precio) AS subTotal
                        FROM facturas f, detalle_factura df
                        WHERE f.id = df.id_factura AND f.eliminado = 0 AND f.rut = ? AND DATE(f.fecha) >= ? AND DATE(f.fecha) <= ? AND f.id_empresa = ?
                        GROUP BY f.id ORDER BY f.fecha_de_creacion DESC', [$rut, $request->input('fechaDesde'), $request->input('fechaHasta'), $request->input('empresa')]
                    );
                }
            } else {
                $facturas = DB::select(
                    'SELECT f.id, f.descripcion, f.email, f.nula, f.eliminado, f.fecha, f.fecha_de_creacion, f.iva, f.numero, f.tipo_factura, f.razon_social, f.direccion, f.rut, f.telefono, f.nombre_contacto, f.id_empresa, ROUND(SUM((df.precio * df.unidades) + df.iva), 0) AS total, SUM(df.unidades * df.precio) AS subTotal
                    FROM facturas f, detalle_factura df
                    WHERE f.id = df.id_factura AND f.eliminado = 0 AND f.rut = ? AND f.id_empresa = ? AND DATE(f.fecha) >= ? AND DATE(f.fecha) <= ?  
                    GROUP BY f.id ORDER BY f.fecha_de_creacion DESC',
                    [$rut, $user->getData()->id_empresa, $request->input('fechaDesde'), $request->input('fechaHasta')]
                );
            }

            return response()->json($facturas, 200);
            
        } catch (\Exception $e) {
            Log::error($e);
            dd($e);
            return response()->json(['error' => 'Intentelo más tarde.'], 500);
        }
    }

    public function makeReporteFacturasProveedor(Request $request) 
    {
        try
        {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
    
            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            $facturas = $request->input('facturas');

            if (!is_array($facturas))
            {
                Log::warning('facturas no es un array');
                return response()->json(['error' => 'No se han podido obtener las facturas para crear el reporte.', 'generate' => false], 400);
            }

            if (count($facturas) == 0)
            {
                Log::info('El arreglo de facturas es 0');
                return response()->json(['error' => 'La cantidad de facturas debe ser mayor a 0'], 400);
            }

            $empresa = $user->getData()->empresa;

            if ($user->getData()->id_rol == 1) { 

                if (!is_numeric($request->input('empresa')))
                {
                    Log::warning('El id de la empresa no es numérico.');
                    return response()->json(['error' => 'No se ha podido obtener el reporte para la empresa.']);
                }          

                if ($request->input('empresa') != 0)
                    $empresa = Entity::find($request->input('empresa'));
            }

            $makeReporte = new MakeReporte();
            $reporte = $makeReporte->makeReporteFacturas($facturas, $user->getData(), 'FACTURAS PROVEEDOR', 2, $request->input('fechaDesde'), $request->input('fechaHasta'), $empresa);
            $fullPath = 'documents/' . $reporte . '.pdf';

            return response()->file($fullPath)->deleteFileAfterSend(true);
        }
        catch (\Exception $e)
        {
            Log::error($e);
            return response()->json(['generate' => false, 'error' => 'Intentelo más tarde'], 500);
        }
    }
    
    public function getReporteArticulos(Request $request, $id) 
    {
        try {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
    
            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            $articulos = [];
            
            if (! is_numeric($id)) {
                return response()->json(['error' => 'El ID del proveedor debe ser numérico.'], 404);
            }

            if (! is_numeric($request->input('empresa'))) {
                return response()->json(['error' => 'El identificador de la empresa no es correcto.'], 404);
            }

            $empresa = $request->input('empresa');

            if ($user->getData()->id_rol == 1) {

                if ($id == 0 && $request->input('empresa') == 0) 
                {
                    $articulos = Articulo::where('eliminado', 0)
                        ->orderBy('created_at', 'desc')
                        ->get()
                        ->each(function (Articulo $a) 
                        {
                            $a->proveedor;
                        }
                    );
                } 
                else if ($id == 0 && $request->input('empresa') != 0) 
                {
                    $articulos = Articulo::where('eliminado', 0)
                        ->whereHas('proveedor', function ($query) use ($empresa)
                        {
                            $query->where('id_empresa', $empresa);
                        })
                        ->orderBy('created_at', 'desc')
                        ->get()
                        ->each(function (Articulo $a) 
                        {
                            $a->proveedor;
                        });
                } 
                else if ($id != 0 && $request->input('empresa') == 0) 
                {
                    $articulos = Articulo::where('eliminado', 0)
                        ->whereHas('proveedor', function ($query) use ($id)
                        {
                            $query->where('id', $id);
                        })
                        ->orderBy('created_at', 'desc')
                        ->get()
                        ->each(function (Articulo $a) 
                        {
                            $a->proveedor;
                        });
                } 
                else if ($id != 0 && $request->input('empresa') != 0) 
                {
                    $articulos = Articulo::where('eliminado', 0)
                        ->whereHas('proveedor', function ($query) use ($id, $empresa)
                        {
                            $query->where('id', $id)->where('id_empresa', $empresa);
                        })
                        ->orderBy('created_at', 'desc')
                        ->get()
                        ->each(function (Articulo $a) 
                        {
                            $a->proveedor;
                        });
                }
            } 
            else 
            {
                $empresa = $user->getData()->id_empresa;

                if ($id == 0) 
                {
                    $articulos = Articulo::where('eliminado', 0)
                        ->whereHas('proveedor', function ($query) use ($id, $empresa)
                        {
                            $query->where('id_empresa', $empresa);
                        })
                        ->orderBy('created_at', 'desc')
                        ->get()
                        ->each(function (Articulo $a) 
                        {
                            $a->proveedor;
                        });
                } 
                else 
                {
                    $articulos = Articulo::where('eliminado', 0)
                        ->whereHas('proveedor', function ($query) use ($id, $empresa)
                        {
                            $query->where('id', $id)->where('id_empresa', $empresa);
                        })
                        ->orderBy('created_at', 'desc')
                        ->get()
                        ->each(function (Articulo $a) 
                        {
                            $a->proveedor;
                        });
                }
            }

            return response()->json($articulos, 200);

        } catch (\Exception $e) {
            return response()->json($e, 500);
        }
    }

    public function makeReporteArticulos(Request $request)
    {
        try
        {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
    
            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            $articulos = $request->input('articulos');

            if (!is_array($articulos))
            {
                Log::warning('articulos no es un array');
                return response()->json(['error' => 'No se han podido obtener los articulos para crear el reporte.', 'generate' => false], 400);
            }

            if (count($articulos) == 0)
            {
                Log::info('El arreglo de articulos es 0');
                return response()->json(['error' => 'La cantidad de articulos debe ser mayor a 0'], 400);
            }
  
            $empresa = $user->getData()->empresa;

            if ($user->getData()->id_rol == 1) { 

                if (!is_numeric($request->input('empresa')))
                {
                    Log::warning('El id de la empresa no es numérico.');
                    return response()->json(['error' => 'No se ha podido obtener el reporte para la empresa.']);
                }          

                if ($request->input('empresa') != 0)
                    $empresa = Entity::find($request->input('empresa'));
            }

            $makeReporte = new MakeReporte();
            $reporte = $makeReporte->makeReporteArticulos($articulos, $user->getData(), 'ARTÍCULOS', $empresa);
            $fullPath = 'documents/' . $reporte . '.pdf';

            return response()->file($fullPath, ['Content-Type' => 'application/pdf'])->deleteFileAfterSend(true);
        }
        catch (\Exception $e)
        {
            Log::error($e);
            return response()->json(['generate' => false, 'error' => 'Intentelo más tarde'], 500);
        }
    }

    public function makeReporteDetalleFactura($id)
    {
        try
        {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
    
            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            $factura = Factura::where('id', $id)->where('eliminado', 0)->first();

            if (count($factura) == 0)
            {
                Log::info('La factura no existe.');
                return response()->json(['error' => 'No existe la factura.'], 400);
            }
            
            $factura->empresa;
            $factura->detallesFactura;

            $impuesto = Impuesto::where('nombre', 'iva')->first();

            if (count($impuesto) == 0) 
            {
                Log::warning('No existe el impuesto iva.');
                return response()->json(['error' => 'No existe el impuesto.'], 400);
            }

            $makeReporte = new MakeReporte();
            $reporte = $makeReporte->makeReporteDetalleFactura($factura, 'Detalle Factura', $user->getData(), $impuesto->valor);

            $fullPath = 'documents/' . $reporte . '.pdf';
            return response()->file($fullPath)->deleteFileAfterSend(true);
        }
        catch (\Exception $e)
        {
            Log::error($e);
            return response()->json(['error' => 'Intentelo más tarde'], 500);
        }
    }

    public function makeReporteDetallePresupuesto($id)
    {
        try
        {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
    
            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            $presupuesto = Presupuesto::where('id', $id)->where('eliminado', 0)->first();

            if (count($presupuesto) == 0)
            {
                Log::info('La presupuesto no existe.');
                return response()->json(['error' => 'No existe la presupuesto.'], 400);
            }
            
            $presupuesto->empresa;
            $presupuesto->detallesPresupuesto;

            $impuesto = Impuesto::where('nombre', 'iva')->first();

            if (count($impuesto) == 0) 
            {
                Log::warning('No existe el impuesto iva.');
                return response()->json(['error' => 'No existe el impuesto.'], 400);
            }

            $makeReporte = new MakeReporte();
            $reporte = $makeReporte->makeReporteDetallePresupuesto($presupuesto, 'Presupuesto', $user->getData(), $impuesto->valor);

            $fullPath = 'documents/' . $reporte . '.pdf';
            return response()->file($fullPath);
            // return response()->file($fullPath)->deleteFileAfterSend(true);
        }
        catch (\Exception $e)
        {
            Log::error($e);
            return response()->json(['error' => 'Intentelo más tarde'], 500);
        }
    }
}  
