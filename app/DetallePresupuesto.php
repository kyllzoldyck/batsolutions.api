<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetallePresupuesto extends Model
{
    protected $table = 'detalle_presupuesto';

    protected $fillable = [
        'id_presupuesto',
        'iva',
        'unidades',
        'descripcion_articulo',
        'precio',
        'codigo_articulo',
        'id_articulo'
    ];

    protected $primaryKey = 'id';

    public $timestamps = true;
    
    public function presupuesto() {
        return $this->belongsTo('App\Presupuesto', 'id_presupuesto');
    }
}
