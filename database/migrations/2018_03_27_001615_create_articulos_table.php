<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articulos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion', 100)->nullable();
            $table->integer('eliminado');
            $table->integer('id_proveedor')->unsigned();
            $table->bigInteger('precio_de_compra');
            $table->bigInteger('precio_de_venta');
            $table->integer('stock');
            $table->timestamps();
            $table->string('codigo', 100)->nullable();
            $table->foreign('id_proveedor')->references('id')->on('proveedores');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articulos');
    }
}
