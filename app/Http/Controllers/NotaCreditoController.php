<?php

namespace App\Http\Controllers;

use App\Http\Requests\NotaCreditoRequest;
use App\Http\Controllers\Api\AuthController;
use App\Factura;
use App\NotaCredito;
use App\Articulo;
use DB;
use Illuminate\Support\Facades\Log;

class NotaCreditoController extends Controller
{
    public function index()
    {
        try {
            Log::info('Hi, getting notas');

            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                Log::warning('Invalid credentials');
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if ($user->getData()->id_rol == 1) {

                Log::info('Getting all notas');
                $notasCredito = NotaCredito::where('eliminado', 0)->orderBy('fecha_de_creacion', 'desc')->get();

                $notasCredito = $notasCredito->each(function (NotaCredito $nota) {
                    $nota->factura->empresa;
                });
            } else {

                Log::info('Getting notas by empresa');
                $notasCredito = NotaCredito::where('eliminado', 0)->orderBy('fecha_de_creacion', 'desc')->get();

                for ($i = 0; $i < count($notasCredito); $i++)
                {
                    if ($notasCredito[$i]->factura->eliminado == 0 && $notasCredito[$i]->factura->empresa->id == $user->getData()->id_empresa)
                    {
                        $notasCredito[$i]->factura->empresa;
                    }
                }
            }

            Log::info('Notas obtained');
            return response()->json($notasCredito, 200);

        } catch (\Exception $e) {
            Log::error($e);
            return response($e, 500);
        }
    }

    public function show($id)
    {
        try {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if (!is_numeric($id)) {
                return response()->json(['error' => 'El ID no es numérico'], 400);
            }

            if ($user->getData()->id_rol == 1) {
                $notaCredito = collect(DB::select(
                    'SELECT nc.id, nc.descripcion, nc.eliminado, nc.fecha, nc.fecha_de_creacion, nc.id_factura, nc.numero, f.id_empresa 
                    FROM nota_de_credito nc, facturas f 
                    WHERE nc.id_factura = f.id AND nc.eliminado = 0 AND f.eliminado = 0 AND nc.id = ? LIMIT 1', [$id]))->first();            
            } else {
                $notaCredito = collect(DB::select(
                    'SELECT nc.id, nc.descripcion, nc.eliminado, nc.fecha, nc.fecha_de_creacion, nc.id_factura, nc.numero, f.id_empresa 
                    FROM nota_de_credito nc, facturas f 
                    WHERE nc.id_factura = f.id AND nc.eliminado = 0 AND f.eliminado = 0 AND f.id_empresa = ? AND nc.id = ? LIMIT 1', [$user->getData()->id_empresa, $id]))->first();            
            }

            return response()->json($notaCredito, 200);
        } catch (\Exception $e) {
            return response()->json($e, 500);
        }
    }

    public function store(NotaCreditoRequest $request)
    {
        try {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            $factura = Factura::find($request->input('id_factura'));

            if (count($factura) == 0) {
                return response()->json(['error' => 'La factura no existe.'], 404);
            } else if ($factura->eliminado == 1) {
                return response()->json(['error' => 'La factura está eliminada.'], 404);
            }

            if ($user->getData()->id_rol != 1) {
                if ($factura->id_empresa != $user->getData()->id_empresa) {
                    return response()->json(['error' => 'No cuenta con permisos para leer esta factura.'], 404);
                }
            }

            $factura->detallesFactura;

            for ($i = 0; $i < count($factura->detallesFactura); $i++) {

                $articulo = Articulo::find($factura->detallesFactura[$i]->id_articulo);
                if (count($articulo) > 0) {
                    # VENTA
                    if ($factura->tipo_factura == 1) {
                        $articulo->stock += $factura->detallesFactura[$i]->unidades;
                    } else if ($factura->tipo_factura == 2) {
                        $articulo->stock -= $factura->detallesFactura[$i]->unidades;
                    }
                    $articulo->save();
                }
            }

            $notaCredito = NotaCredito::where('id_factura', $factura->id)->first();

            if (count($notaCredito) > 0) {
                return response()->json(['error' => 'Ya existe una nota de credito asociada a la factura.'], 400);
            }

            $notaCredito = new NotaCredito();
            $notaCredito->descripcion = mb_strtolower(trim($request->input('descripcion')), 'UTF-8');
            $notaCredito->numero = $request->input('numero');
            $notaCredito->fecha = date($request->input('fecha'));
            $notaCredito->id_factura = $request->input('id_factura');
            $notaCredito->eliminado = 0;
            $notaCredito->fecha_de_creacion = date('Y-m-d H:i:s');
            $notaCredito->save();



            $factura->nula = 1;
            $factura->save();

            return response()->json($notaCredito, 200);

        } catch (\Exception $e) {
            return response()->json($e, 500);
        }
    }

    public function update(NotaCreditoRequest $request, $id)
    {
        try {

            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if (!is_numeric($id)) {
                return response()->json(['error' => 'El ID no es numérico.'], 400);
            }

            if ($user->getData()->id_rol == 1) {
                $notaCredito = NotaCredito::where('id', $id)->where('eliminado', 0)->first();
            } else {
                $notaCredito = collect(DB::select(
                    'SELECT nc.id, nc.descripcion, nc.eliminado, nc.fecha, nc.fecha_de_creacion, nc.id_factura, nc.numero, f.id_empresa 
                    FROM nota_de_credito nc, facturas f 
                    WHERE nc.id_factura = f.id AND nc.eliminado = 0 AND f.id_empresa = ? AND nc.id = ? LIMIT 1', [$user->getData()->id_empresa, $id]))->first();            

                $notaCredito = NotaCredito::find($notaCredito->id);
            }

            if (count($notaCredito) == 0) {
                return response()->json(['error' => 'La nota de credito no existe.'], 404);
            }

            $notaCredito->descripcion = mb_strtolower(trim($request->input('descripcion')), 'UTF-8');
            $notaCredito->fecha = date($request->input('fecha'));

            $notaCredito->save();

            return response()->json($notaCredito, 200);

        } catch (\Exception $e) {
            return response()->json($e, 500);
        }
    }

    public function destroy($id)
    {
        try {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if (!is_numeric($id)) {
                return response()->json(['error' => 'El ID no es numérico.'], 400);
            }

            if ($user->getData()->id_rol == 1) {
                $notaCredito = NotaCredito::where('id', $id)
                    ->where('eliminado', 0)->first();
            } else {
                $notaCredito = collect(DB::select(
                    'SELECT nc.id, nc.descripcion, nc.eliminado, nc.fecha, nc.fecha_de_creacion, nc.id_factura, nc.numero, f.id_empresa 
                    FROM nota_de_credito nc, facturas f 
                    WHERE nc.id_factura = f.id AND nc.eliminado = 0 AND f.id_empresa = ? AND nc.id = ? LIMIT 1',
                    [$user->getData()->id_empresa, $id]
                ))->first();

                $notaCredito = NotaCredito::find($notaCredito->id); 
            }

            if (count($notaCredito) == 0) {
                return response()->json(['error' => 'La nota de credito no existe.'], 404);
            }

            $notaCredito->eliminado = 1;
            $notaCredito->save();

            return response('Nota de credito eliminada.', 204);

        } catch (\Exception $e) {
            return response()->json($e, 500);
        }
    }

    public function getNotaCreditoByNumero($numero)
    {
        try {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if (!is_numeric($numero)) {
                return response()->json(['error' => 'El número de la nota de credito no es correcta.'], 404);
            }

            if ($user->getData()->id_rol == 1) {
                $nota = DB::select(
                    'SELECT nc.id, nc.descripcion, nc.eliminado, nc.fecha, nc.fecha_de_creacion, nc.id_factura, nc.numero
                    FROM nota_de_credito nc, facturas f
                    WHERE nc.id_factura = f.id AND nc.eliminado = 0 AND nc.numero = ?', [$numero]);
            } else {
                $nota = DB::select(
                    'SELECT nc.id, nc.descripcion, nc.eliminado, nc.fecha, nc.fecha_de_creacion, nc.id_factura, nc.numero
                    FROM nota_de_credito nc, facturas f
                    WHERE nc.id_factura = f.id AND f.id_empresa = ? AND nc.eliminado = 0 AND nc.numero = ?', [$user->getData()->id_empresa, $numero]);
            }

            return response()->json($nota, 200);

        } catch (\Exception $e) {

        }
    }
}
