<?php 
    
namespace App\Repository;

use App\Presupuesto;
use App\DetallePresupuesto;

class PresupuestoTemplateEmail {
    

    public function presupuestoTemplate(Presupuesto $presupuesto)
    {
        return '
            <div style="padding: 20px 50px;">
                Estimado ' . $presupuesto->nombre . ',
                <br>
                ' . $presupuesto->descripcion . '
            </div>
        ';
    }

}