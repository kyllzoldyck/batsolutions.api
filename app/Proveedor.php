<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{

    protected $table = 'proveedores';

    protected $fillable = [
        'direccion',
        'email',
        'eliminado',
        'nombre_contacto',
        'razon_social',
        'rut',
        'telefono',
        'id_empresa',
        'giro'
    ];

    public $timestamps = true;

    public function articulos() {
        return $this->hasMany('App\Articulo', 'id_proveedor', 'id');
    }

    public function empresa() {
        return $this->belongsTo('App\Entity', 'id_empresa');
    }

}
