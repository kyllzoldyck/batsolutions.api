<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetallePresupuestoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_presupuesto', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_presupuesto')->unsigned();
            $table->decimal('iva', 20, 2);
            $table->integer('unidades');
            $table->string('descripcion_articulo')->nullable();
            $table->bigInteger('precio');
            $table->string('codigo_articulo', 100)->nullable();
            $table->integer('id_articulo')->unsigned();
            $table->timestamps();
            
            $table->foreign('id_presupuesto')->references('id')->on('presupuestos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_presupuesto');
    }
}
