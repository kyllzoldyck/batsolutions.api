<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('apellidos', 150);
            $table->string('email', 50);
            $table->integer('activo');
            $table->integer('id_rol')->unsigned();
            $table->integer('eliminado');
            $table->string('nombre', 100);
            $table->string('password');
            $table->string('username', 10)->unique();
            $table->timestamps();
            $table->string('remember_token', 100)->nullable();
            $table->integer('id_empresa')->unsigned();

            $table->foreign('id_rol')->references('id')->on('rols');
            $table->foreign('id_empresa')->references('id')->on('entities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
