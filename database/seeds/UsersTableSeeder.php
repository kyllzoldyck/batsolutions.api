<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usuarios')->insert([
            'apellidos' => 'Bat Solutions',
            'email' => 'contacto@batsolutions.cl',
            'activo' => 1,
            'id_rol' => 1,
            'eliminado' => 0,
            'nombre' => 'Administrador',
            'password' => bcrypt('123456'),
            'username' => 'Admin',
            'remember_token' => '',
            'id_empresa' => 1
        ]);
    }
}
