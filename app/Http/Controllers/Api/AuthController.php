<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;
use DB;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;


class AuthController extends Controller
{

    public function authenticate(Request $request)
    {
        $user = User::where(DB::raw('BINARY `username`'), $request->input('username'))
            ->where('eliminado', 0)
            ->where('activo', 1)
            ->first();

        if (count($user) > 0 && Hash::check($request->input('password'), $user->password)) {

            $credentials = $request->only('username', 'password');

            try {
                if (!$token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'Las credenciales del usuario no son correctas.'], 401);
                }
            } catch (JWTException $e) {
                return response()->json(['error' => 'Algo ha ocurrido mal!'], 500);
            }

            return response()->json(compact('token'));
        }
        return response()->json(['error' => 'Credenciales inválidas.'], 404);
    }

    public function getAuthenticatedUser()
    {
        try {
            if (!$usuario = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }

        $usuario = User::find($usuario->id);
        $usuario->rol;
        $usuario->empresa;

        return response()->json($usuario, 200);
    }

    public function updatePassword(Request $request)
    {
        try {
            
            if ($request->input('password') !== $request->input('password_repeat')) {
                return response()->json(['error' => 'Las contraseñas no coinciden.'], 404);
            }

            $user = User::where('username', $request->input('username'))
                ->where('eliminado', 0)->first();
            
            if (count($user) == 0) {
                return response()->json(['error' => 'El usuario no existe.'], 404);
            }

            $user->password = bcrypt($request->input('password'));
            $user->save();

            return response()->json(['success' => 'Contraseña modificada correctamente.'], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Ha ocurrido un error, por favor contactar con el administrador.'], 500);
        }
    }
}
