<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticuloRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'descripcion' => 'required|max:100',
            'id_proveedor' => 'numeric|required',
            'precio_de_compra' => 'numeric|required',
            'precio_de_venta' => 'numeric|required',
            'stock' => 'numeric|required',
            'codigo' => 'max:100|required'
        ];
    }

    public function messages()
    {
        return [
            'descripcion.required' => 'El campo descripción es obligatorio.',
            'descripcion.max:100' => 'La descripción supera el máximo de caracteres',

            'id_proveedor.numeric' => 'El valor de proveedor no es correcto.',
            'id_proveedor.required' => 'El proveedor es obligatorio.',
            
            'precio_de_compra.numeric' => 'El valor de precio de compra no es correcto.',
            'precio_de_compra.required' => 'El precio de compra es obligatorio.',
            
            'precio_de_venta.numeric' => 'El valor de precio de venta no es correcto.',
            'precio_de_venta.required' => 'El precio de venta es obligatorio.',
            
            'stock.numeric' => 'El valor de stock no es correcto.',
            'stock.required' => 'El campo stock es obligatorio.',

            'codigo.max:100' => 'El código supera el máximo de caracteres.',
            'codigo.required' => 'El código es obligatorio.'
        ];
    }
}
