<?php

use Faker\Generator as Faker;
use \Freshwork\ChileanBundle\Rut;

$factory->define(App\Cliente::class, function (Faker $faker) {
    
    $random_number = rand(1000000, 25000000);
    $rut = new Rut($random_number);

    return [
        'direccion' => $faker->address,
        'email' => $faker->email,
        'eliminado' => 0,
        'nombre_contacto' => $faker->word,
        'razon_social' => $faker->name,
        'rut' => $rut->fix()->format(Rut::FORMAT_WITH_DASH),
        'telefono' => 12345678,
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s'),
        'id_empresa' => rand(1, 8),
        'giro' => $faker->name
    ];
});
