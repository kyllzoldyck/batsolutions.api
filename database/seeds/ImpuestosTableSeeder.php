<?php

use Illuminate\Database\Seeder;

class ImpuestosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('impuestos')->insert([
            'nombre' => 'iva',
            'valor' => 0.19,
            'date' => date('Y-m-d'),
            'state' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
