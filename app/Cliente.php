<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{

    protected $table = 'clientes';

    protected $fillable = [
        'direccion',
        'email',
        'eliminado',
        'nombre_contacto',
        'razon_social',
        'rut',
        'telefono',
        'id_empresa',
        'giro'
    ];

    public $timestamps = true;

    public function facturas() {
        return $this->hasMany('App\Factura', 'id_cliente', 'id');
    }

    public function empresa() {
        return $this->belongsTo('App\Entity', 'id_empresa');
    }
}
