<?php

namespace App\Http\Controllers;

use App\Entity;
use Illuminate\Http\Request;
use App\Http\Requests\EntityRequest;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Api\AuthController;

class EntityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try
        {
            Log::info('Hi, getting entities');
        
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
    
            if ($user->getStatusCode() != 200) 
            {
                Log::warning('Credenciales incorrectas');
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if ($user->getData()->id_rol == 1) 
            {
                Log::info('Admin: Getting Entities');
                $entities = Entity::where('state', '1')->get();
            }
            else 
            {
                Log::info('Getting Entity');
                $entities = Entity::where('state', '1')->where('id', $user->getData()->id_empresa)->get();
            }
            
        }
        catch (\Exception $e) 
        {
            Log::error($e);
            return response()->json(['get' => false], 204);
        }

        Log::info('Entities obtained');
        return response()->json($entities, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EntityRequest $request)
    {
        $entity = new Entity();

        Log::info('Hi, creating entity...');

        $entity->name = mb_strtolower(trim($request->input('name')), 'UTF-8');
        $entity->razon_social = mb_strtolower(trim($request->input('razon_social')), 'UTF-8');
        $entity->rut = mb_strtoupper(trim($request->input('rut')), 'UTF-8');
        $entity->url_img = trim($request->input('url_img'));
        $entity->created_at = date('Y-m-d H:i:s');
        $entity->updated_at = date('Y-m-d H:i:s');
        $entity->state = $request->input('state');
        $entity->email = $request->input('email');
        $entity->giro = $request->input('giro');

        try
        {
            Log::info('Saving entity as ' . $entity->name);
            $entity->save();
        }
        catch (\Exception $e) 
        {
            Log::error($e);
            return response()->json(['created', false], 202);
        }

        Log::info('Entity created as ' . $entity->name);
        return response()->json(['created' => true], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity  $entity
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Log::info('Hi, getting single entity.');

        if (!is_numeric($id))
        {
            Log::warning('El id de la entidad no es numérico.');
            return response()->json(['get' => false], 422);
        }

        try
        {
            $entity = Entity::where('state', 1)->where('id', $id)->first();
        }
        catch (\Exception $e)
        {
            Log::error($e);
            return response()->json(['get' => false], 204);
        }

        Log::info('Entity obtained ' . $entity->id);
        return response()->json($entity, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity  $entity
     * @return \Illuminate\Http\Response
     */
    public function update(EntityRequest $request, $id)
    {
        Log::info('Hi, updating entity');

        if (!is_numeric($id)) 
        {
            Log::warning('El id de la entidad no es numérico.');
            return response()->json(['updated' => false, 'error' => 'El id de la entidad no es numérico.'], 422);
        } 
        
        $entity = Entity::find($id);

        if (count($entity) == 0) 
        {
            Log::warning('No se ha encontrado la entidad.');
            return response()->json(['updated' => false, 'error' => 'No se ha encontrado la entidad.'], 204);
        }

        $entity->name = mb_strtolower(trim($request->input('name')), 'UTF-8');
        $entity->razon_social = mb_strtolower(trim($request->input('razon_social')), 'UTF-8');
        $entity->url_img = trim($request->input('url_img'));
        $entity->updated_at = date('Y-m-d H:i:s');
        $entity->state = $request->input('state');
        $entity->email = $request->input('email');
        $entity->giro = $request->input('giro');

        try 
        {
            Log::info('Updating entity ' . $id);
            $entity->save();
        }
        catch (\Exception $e) 
        {
            Log::error($e);
            return response()->json(['updated' => false], 202);
        }

        Log::info('Entity updated');
        return response()->json(['updated' => true], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity  $entity
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Log::info('Hi, deleting entity');

        if (!is_numeric($id)) 
        {
            Log::warning('El id de la entidad no es numérico.');
            return response()->json(['deleted' => false, 'error' => 'El id de la entidad no es numérico.'], 422);
        } 
            
        $entity = Entity::find($id);

        if (count($entity) == 0) 
        {
            Log::warning('No se ha encontrado la entidad.');
            return response()->json(['deleted' => false, 'error' => 'No se ha encontrado la entidad.'], 204);
        }

        try
        {
            $entity->state = 0;
            $entity->save();
        }
        catch (\Exception $e)
        {
            Log::error($e);
            return response()->json(['deleted' => false], 204);
        }

        Log::info('Entity ' . $entity->id . ' deleted.');
        return response()->json(['deleted' => true], 200);
    }
}
