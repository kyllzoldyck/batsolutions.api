<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Presupuesto;

class PresupuestoMail extends Mailable
{
    use Queueable, SerializesModels;

    public $presupuesto;
    public $file;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Presupuesto $presupuesto, $file)
    {
        $this->presupuesto = $presupuesto;
        $this->file = $file;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        return $this
            ->from($this->presupuesto->empresa->email, 'name test')
            ->subject($this->presupuesto->asunto)
            ->view('emails.presupuesto')
            ->attach($this->file, [ 'as' => 'presupuesto.pdf', 'mime' => 'application/pdf' ]);

        // dd(config('mail.username'));

        // return $d;
    }
}
