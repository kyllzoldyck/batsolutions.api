<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use App\Mail\PasswordResetMail;
use App\User;
use DB;
use Illuminate\Support\Facades\Mail;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function __invoke(Request $request)
    {
        $this->validateEmail($request);

        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        if ($response == Password::RESET_LINK_SENT)
        {

            $token = DB::table('password_resets')->first(['token']);
            $user = User::where('email', $request->input('email'))->first();

            $passwordMail = Mail::to($request->input('email'))->send(new PasswordResetMail($token, $user));
        }

        return $response == Password::RESET_LINK_SENT
            ? response()->json(['message' => 'Se ha enviado un correo para recuperar su contraseña.', 'status' => true], 201)
            : response()->json(['message' => 'No se ha podido envíar el correo.'], 401);
    }
}
