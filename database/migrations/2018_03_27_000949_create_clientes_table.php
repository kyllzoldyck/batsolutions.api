<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('direccion', 200);
            $table->string('email', 100)->unique();
            $table->integer('eliminado');
            $table->string('nombre_contacto', 200)->nullable();
            $table->string('razon_social');
            $table->string('rut', 10);
            $table->string('telefono', 15);
            $table->integer('id_empresa')->unsigned();
            $table->timestamps();
            $table->string('giro', 200);
            
            $table->foreign('id_empresa')->references('id')->on('entities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
