<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PresupuestoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fecha_expiracion' => 'required|date',
            'nombre' => 'required|string|max:100|min:5',
            'email' => 'required|string|email',
            'asunto' => 'required|string|max:100|min:5',
            'descripcion' => 'required|string|max:500|min:5',
            'detalles_presupuesto' => 'required|array',
            'cliente_rut' => 'required|string',
            'exenta' => 'required|boolean'
        ];
    }

    public function messages()
    {
        return [
            'fecha_expiracion.required' => 'La fecha de expiración es obligatoria.',
            'fecha_expiracion.date' => 'La fecha de expiración no es correcta.',
            
            'nombre.required' => 'El nombre es obligatorio.',
            'nombre.string' => 'El nombre no es correcto.',
            'nombre.max' => 'El nombre debe ser de un máximo de 100 caracteres.',
            'nombre.min' => 'El nombre debe ser de un mínimo de 5 caracteres.',

            'email.required' => 'El email es obligatorio.',
            'email.string' => 'El email no es correcto',
            'email.email' => 'El email no es correcto',

            'asunto.required' => 'El asunto es obligatorio.',
            'asunto.string' => 'El asunto no es correcto',
            'asunto.max' => 'El asunto debe ser de un máximo de 100 caracteres.',
            'asunto.min' => 'El asunto debe ser de un mínimo de 5 caracteres.',

            'descripcion.required' => 'La descripción es obligatoria.',
            'descripcion.string' => 'La descripción no es correcta',
            'descripcion.max' => 'La descripción debe ser de un máximo de 500 caracteres.',
            'descripcion.min' => 'La descripción debe ser de un mínimo de 5 caracteres.',

            'cliente_rut.required' => 'El cliente es obligatorio.',
            'cliente_rut.string' => 'El cliente no es correcto.',

            'detalle_presupuesto.required' => 'El presupuesto no cuenta con un detalle de artículos.',
            'detalle_presupuesto.array' => 'El detalle del presupuesto no es correcto.',

            'exenta.required' => 'El tipo de exenta no es válido.',
            'exenta.boolean' => 'El tipo de exenta no es válido.'
        ];
    }
}
