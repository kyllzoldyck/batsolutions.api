<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResetPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required|min:6',
            'token' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'password.required' => 'La contraseña es obligatoria.',
            'password.confirmed' => 'Las contraseñas no coinciden.',
            'password.min' => 'La contraseña debe tener al menos 6 caracteres.',

            'password_confirmation.required' => 'La contraseña es obligatoria.',
            'password_confirmation.min' => 'La contraseña debe tener al menos 6 caracteres',

            'token.required' => 'El token de autorización no es válido.'
        ];
    }
}
