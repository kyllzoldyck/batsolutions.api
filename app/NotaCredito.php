<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotaCredito extends Model
{
    
    protected $table = 'nota_de_credito';

    protected $fillable = [
        'descripcion',
        'eliminado',
        'fecha',
        'fecha_de_creacion',
        'id_factura',
        'numero'
    ];

    public $timestamps = false;

    public function factura() {
        return $this->belongsTo('App\Factura', 'id_factura');
    }

}
