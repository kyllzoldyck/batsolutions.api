<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Api\AuthController;
use App\User;
use App\Entity;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    public function index()
    {
        try {

            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }
            # Solo el admin tiene permisos para ver los usuarios
            if ($user->getData()->id_rol != 1) {
                return response()->json(['error' => 'No cuenta con permisos.'], 203);
            }

            $users = User::where('eliminado', 0)->orderBy('created_at', 'desc')->get();

            $users->each(function (User $user) {
                $user->rol;
                $user->empresa;
                $user->name_activo = $user->activo == 1 ? 'activo' : 'inactivo';
            });

            return response()->json($users, 200);

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['get' => false, 'error' => 'Intente más tarde por favor.'], 500);
        }
    }

    public function store(UserRequest $request)
    {
        try {
            Log::info('Hi, creating user');

            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                Log::warning('Credenciales incorrectas.');
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }
            # Solo el admin tiene permisos para agregar los usuarios
            if ($user->getData()->id_rol != 1) {
                Log::warning('No cuenta con permisos.');
                return response()->json(['error' => 'No cuenta con permisos.'], 203);
            }

            $user = User::where('username', $request->input('username'))->get();

            if (count($user) > 0)
            {
                Log::info('Ya existe un usuario con ese nombre de usuario.');
                return response('Ya existe un usuario con ese nombre de usuario.', 200);
            }

            $user = new User();
            $user->apellidos = mb_strtolower(trim($request->input('apellidos')), 'UTF-8');
            $user->email = mb_strtolower(trim($request->input('email')), 'UTF-8');
            $user->activo = trim($request->input('activo'));
            $user->id_rol = trim($request->input('id_rol'));
            $user->eliminado = 0;
            $user->nombre = mb_strtolower(trim($request->input('nombre')), 'UTF-8');
            $user->password = bcrypt($request->input('password'));
            $user->username = trim($request->input('username'));
            $user->created_at = date('Y-m-d H:i:s');
            $user->updated_at = date('Y-m-d H:i:s');
            $user->remember_token = '';
            $user->id_empresa = trim($request->input('id_empresa'));
            $user->save();
            
            Log::info('User created ' . $user->username);
            return response()->json($user, 200);
            
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['error' => 'Intentelo más tarde.'], 500);
        }
    }

    public function update(UserRequest $request, $id)
    {
        try {

            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }
            # Solo el admin tiene permisos para actualizar los usuarios
            if ($user->getData()->id_rol != 1) {
                return response()->json(['error' => 'No cuenta con permisos.'], 203);
            }

            $user = User::find($id);

            if (count($user) == 0)
                return response()->json(['error' => 'El usuario no existe en el sistema.'], 404);

            $user->apellidos = mb_strtolower(trim($request->input('apellidos')), 'UTF-8');
            $user->email = mb_strtolower(trim($request->input('email')), 'UTF-8');
            $user->activo = trim($request->input('activo'));
            $user->id_rol = trim($request->input('id_rol'));
            $user->eliminado = 0;
            $user->nombre = mb_strtolower(trim($request->input('nombre')), 'UTF-8');
            # !empty($request->input('password')) ? $user->password = bcrypt($request->input('password')) : null;
            $user->username = trim($request->input('username'));
            $user->updated_at = date('Y-m-d H:i:s');
            $user->remember_token = '';
            $user->id_empresa = trim($request->input('id_empresa'));
            $user->save();

            return response()->json($user, 200);

        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['error' => 'Intentelo más tarde.'], 500);
        }
    }

    public function destroy($id)
    {
        try {

            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }
            # Solo el admin tiene permisos para eliminar los usuarios
            if ($user->getData()->id_rol != 1) {
                return response()->json(['error' => 'No cuenta con permisos.'], 203);
            }

            if (!is_numeric($id))
                return response()->json(['errors' => 'El ID no es numérico.', 200]);

            $user = User::find($id);

            if (count($user) == 0)
                return response('El usuario no existe en el sistema.', 404);
            
            # 1 = Eliminado, 0 = No eliminado
            $user->eliminado = 1;
            $user->save();

            return response('Usuario eliminado.', 204);
        } catch (\Exception $e) {
            return response()->json($e, 404);
        }
    }
}
