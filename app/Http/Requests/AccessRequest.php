<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AccessRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id_print_menu' => 'required|numeric|gt:0',
            'id_entity' => 'required|numeric|gt:0',
            'id_rol' => 'required|numeric|gt:0',
            'state' => 'required|numeric|boolean'
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'id_print_menu.required' => 'Debe escoger al menos un menu para asignar.',
            'id_print_menu.numeric' => 'El módulo no es correcto.',
            'id_print_menu.gt' => 'Debe seleccionar un menu.',
            
            'id_entity.required' => 'La empresa es obligatoria.',
            'id_entity.numeric' => 'La empresa no es correcta.',
            'id_entity.gt' => 'Debe seleccionar una empresa.',

            'id_rol.required' => 'El rol es obligatorio.',
            'id_rol.numeric' => 'El rol no es correcto.',
            'id_rol.gt' => 'Debe seleccionar un rol.',

            'state.required' => 'El estado es obligatorio.',
            'state.numeric' => 'El estado es de tipo numérico.',
            'state.boolean' => 'El estado debe ser 1 o 0.'
        ];
    }
}
