<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\AuthController;
use App\Http\Requests\ClienteRequest;
use App\Cliente;
use App\Factura;
use Illuminate\Support\Facades\Log;
use \Freshwork\ChileanBundle\Rut;

class ClienteController extends Controller
{
    public function index()
    {
        try {

            Log::info('Getting clients');

            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) 
            {
                Log::warning('Credenciales incorrectas');    
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if ($user->getData()->id_rol == 1) 
            {
                Log::info('Admin: Getting clients');
                $clientes = Cliente::where('eliminado', 0)->orderBy('created_at', 'desc')->get();
            } 
            else 
            {
                Log::info('Getting clients');
                $clientes = Cliente::where('eliminado', 0)
                    ->where('id_empresa', $user->getData()->id_empresa)
                    ->orderBy('created_at', 'desc')
                    ->get();
            }

            $clientes->each(function (Cliente $cliente) {
                $cliente->empresa;
            });

        } catch (\Exception $e) {
            return response()->json($e, 404);
        }

        Log::info('Clientes obtained');
        return response()->json($clientes, 200);
    }

    public function store(ClienteRequest $request)
    {
        try {

            Log::info('Creating client');

            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) 
            {
                Log::warning('Credenciales incorrectas');
                return response()->json(['error' => 'Credenciales incorrectas.'], 401);
            }

            $rut = Rut::parse(mb_strtoupper(trim($request->input('rut')), 'UTF-8'))->format(Rut::FORMAT_WITH_DASH);

            $id_empresa = $user->getData()->id_rol == 1 ? trim($request->input('id_empresa')) : trim($user->getData()->id_empresa);            

            $cliente = Cliente::where('rut', $rut)
                ->where('id_empresa', $id_empresa)
                ->first();

            if (count($cliente) > 0)
            {
                Log::info('El cliente ya existe en el sistema');
                return response()->json(['error' => 'El Cliente ya existe en el sistema.'], 400);
            }

            $cliente = new Cliente();
            $cliente->direccion = mb_strtolower(trim($request->input('direccion')), 'UTF-8');
            $cliente->email = mb_strtolower(trim($request->input('email')), 'UTF-8');
            $cliente->eliminado = 0;
            $cliente->nombre_contacto = mb_strtolower(trim($request->input('nombre_contacto')), 'UTF-8');
            $cliente->razon_social = mb_strtolower(trim($request->input('razon_social')), 'UTF-8');
            $cliente->rut = $rut;
            $cliente->telefono = trim($request->input('telefono'));
            $cliente->giro = mb_strtolower(trim($request->input('giro')), 'UTF-8');
            $cliente->id_empresa = $id_empresa;
            $cliente->save();

            Log::info('Client created');
            return response()->json($cliente, 200);
        } 
        catch (\Exception $e) {
            Log::error($e);
            return response()->json(['error' => 'Intentelo más tarde.'], 500);
        }
    }

    public function show($id)
    {
        try {
            $auth = new AuthController();
            if ($auth->getAuthenticatedUser()->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if (!is_numeric($id))
                return response()->json(['errors' => 'El ID no es numérico.', 200]);

            # 1 = Eliminado, 0 = No eliminado
            $cliente = Cliente::where('id', $id)->where('eliminado', 0)->get();

            return response()->json($cliente, 200);
        } catch (\Exception $e) {
            return response()->json($e, 500);
        }
    }

    public function update(ClienteRequest $request, $id)
    {
        try {
            Log::info('Updating client');

            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();
            
            if ($user->getStatusCode() != 200) 
            {
                Log::warning('Credenciales incorrectas');
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            $cliente = Cliente::find($id);

            if (count($cliente) == 0)
            {
                Log::warning('El cliente no existe en el sistema');
                return response('El Cliente no existe en el sistema.', 404);
            }

            $clienteExistente = Cliente::where('rut', mb_strtolower(trim($request->input('rut')), 'UTF-8'))
                ->where('id_empresa', $request->input('id_empresa') == $cliente->id_empresa ? '' : $request->input('id_empresa'))
                ->first();

            if (count($clienteExistente) > 0)
            {
                Log::info('Ya existe un cliente con este RUT');
                return response()->json(['error' => 'Ya existe un cliente con este RUT.'], 404);
            }
           
            $cliente->direccion = mb_strtolower(trim($request->input('direccion')), 'UTF-8');
            $cliente->email = mb_strtolower(trim($request->input('email')), 'UTF-8');
            $cliente->eliminado = 0;
            $cliente->nombre_contacto = mb_strtolower(trim($request->input('nombre_contacto')), 'UTF-8');
            $cliente->razon_social = mb_strtolower(trim($request->input('razon_social')), 'UTF-8');
            # $proveedor->rut = $request->input('rut');
            $cliente->telefono = trim($request->input('telefono'));
            $cliente->giro = mb_strtolower(trim($request->input('giro')), 'UTF-8');
            $cliente->id_empresa = $user->getData()->id_rol == 1 ? trim($request->input('id_empresa')) : trim($user->getData()->id_empresa);
            $cliente->save();

            Log::info('Client updated');
            return response()->json($cliente, 200);
        } catch (\Exception $e) 
        {
            Log::error($e);
            return response()->json($e, 500);
        }
    }

    public function destroy($id)
    {
        try {

            $auth = new AuthController();
            if ($auth->getAuthenticatedUser()->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if (!is_numeric($id))
                return response()->json(['error' => 'El ID no es numérico.'], 200);

            $cliente = Cliente::find($id);

            if (count($cliente) == 0)
                return response()->json(['error' => 'El cliente no existe en el sistema.'], 404);

            $facturas = Factura::where('rut', $cliente->rut)
                ->where('id_empresa', $cliente->id_empresa)
                ->where('eliminado', 0)
                ->get();

            if (count($facturas) > 0) {
                return response()->json(['error' => 'El cliente tiene facturas asociadas. No puede ser eliminado.'], 404);
            }   

            # 1 = Eliminado, 0 = No eliminado
            $cliente->eliminado = 1;
            $cliente->save();

            return response('Cliente eliminado.', 204);
        } catch (\Exception $e) {
            return response()->json($e, 500);
        }
    }

    public function getClientesByIdEmpresa($id)
    {
        try {

            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if (!is_numeric($id)) {
                return response()->json(['error' => 'El ID debe ser numérico.'], 404);
            }

            $clientes = [];

            if ($user->getData()->id_rol == 1) {
                $clientes = Cliente::where('eliminado', 0)->orderBy('created_at', 'desc')->where('id_empresa', $id)->get();
            }
            else
            {
                $clientes = Cliente::where('eliminado', 0)->orderBy('created_at', 'desc')->where('id_empresa', $user->getData()->id_empresa)->get();
            }

            return response()->json($clientes, 200);

        } catch (\Exception $e) {
            return response()->json($e, 500);
        }
    }

    public function getClienteByRut($rut)
    {
        try {
            $auth = new AuthController();
            $user = $auth->getAuthenticatedUser();

            if ($user->getStatusCode() != 200) {
                return response()->json(['error' => 'Credenciales incorrectas.'], 404);
            }

            if (empty($rut)) {
                return response()->json(['error' => 'El rut es obligatorio.'], 404);
            }

            $clientes = [];

            if ($user->getData()->id_rol == 1) {
                $clientes = Cliente::where('rut', $rut)->where('eliminado', 0)->first();
            } else {
                $clientes = Cliente::where('rut', $rut)->where('eliminado', 0)->where('id_empresa', $user->getData()->id_empresa)->first();
            }

            return response()->json($clientes, 200);

        } catch (\Exception $e) {

        }
    }
}
